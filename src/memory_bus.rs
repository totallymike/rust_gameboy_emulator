pub struct MemoryBus {
    memory: [u8; 0xFFFF],
}

impl MemoryBus {
    pub fn new() -> MemoryBus {
        MemoryBus {
            memory: [0; 0xFFFF],
        }
    }

    pub fn read_byte(&self, address: u16) -> u8 {
        self.memory[address as usize]
    }

    pub fn read_word(&self, address: u16) -> u16 {
        let lower_half = self.memory[address as usize];
        let upper_half = self.memory[address as usize + 1];
        (upper_half as u16) << 8 | lower_half as u16
    }

    pub fn write_byte(&mut self, address: u16, value: u8) {
        self.memory[address as usize] = value;
    }

    pub fn write_word(&mut self, address: u16, value: u16) {
        let [lower_half, upper_half] = value.to_le_bytes();
        self.memory[address as usize] = lower_half;
        self.memory[address as usize + 1] = upper_half;
    }

    pub fn handle_to_address(&mut self, address: u16) -> &mut u8 {
        &mut self.memory[address as usize]
    }
}
