use super::flags::Flags;

pub struct Registers {
    pub(in super) a: u8,
    pub(in super) f: Flags,
    pub(in super) b: u8,
    pub(in super) c: u8,
    pub(in super) d: u8,
    pub(in super) e: u8,
    pub(in super) h: u8,
    pub(in super) l: u8,
}

impl Registers {
    pub(in crate::cpu) fn new() -> Registers {
      Registers {
        a: 0,
        f: Flags::empty(),
        b: 0,
        c: 0,
        d: 0,
        e: 0,
        h: 0,
        l: 0,
      }
    }

    pub fn init() -> Registers {
        Registers {
            a: 0x01,
            f: (Flags::ZERO | Flags::CARRY | Flags::HALF_CARRY),
            b: 0x00,
            c: 0x13,
            d: 0x00,
            e: 0xD8,
            h: 0x01,
            l: 0x4D,
        }
    }

    pub fn flags(&self) -> Flags {
        self.f
    }

    pub(in super) fn set_flag(&mut self, flag: Flags, value: bool) {
        self.f.set(flag, value);
    }

    pub fn get_af(&self) -> u16 {
        (self.a as u16) << 8 | self.f.bits() as u16
    }

    pub fn set_af(&mut self, value: u16) {
        let [lower_half, upper_half] = value.to_le_bytes();
        self.a = upper_half;
        self.f = Flags::from_bits_truncate(lower_half);
    }

    pub fn get_bc(&self) -> u16 {
        (self.b as u16) << 8 | self.c as u16
    }

    pub fn set_bc(&mut self, value: u16) {
        self.b = ((value & 0xFF00) >> 8) as u8;
        self.c = value as u8;
    }

    pub fn get_de(&self) -> u16 {
        (self.d as u16) << 8 | self.e as u16
    }

    pub fn set_de(&mut self, value: u16) {
        self.d = (value >> 8) as u8;
        self.e = value as u8;
    }

    pub fn get_hl(&self) -> u16 {
        (self.h as u16) << 8 | self.l as u16
    }

    pub fn set_hl(&mut self, value: u16) {
        self.h = (value >> 8) as u8;
        self.l = value as u8;
    }

    pub fn get_a(&self) -> u8 {
        self.a
    }

    pub fn get_f(&self) -> u8 {
        self.f.bits()
    }

    pub fn get_b(&self) -> u8 {
        self.b
    }

    pub fn get_c(&self) -> u8 {
        self.c
    }

    pub fn get_d(&self) -> u8 {
        self.d
    }

    pub fn get_e(&self) -> u8 {
        self.e
    }

    pub fn get_h(&self) -> u8 {
        self.h
    }

    pub fn get_l(&self) -> u8 {
        self.l
    }
}

#[cfg(test)]
mod tests {
    use super::Registers;

    #[test]
    fn register_access() {
        let registers = Registers::init();
        assert_eq!(registers.get_a(), 0x01u8);
        assert_eq!(registers.get_f(), 0xB0u8);
        assert_eq!(registers.get_af(), 0x01B0);
        assert_eq!(registers.get_bc(), 0x0013);
        assert_eq!(registers.get_de(), 0x00D8);
        assert_eq!(registers.get_hl(), 0x014D);
    }

    #[test]
    fn register_mutation() {
        let mut registers = Registers::init();
        registers.set_bc(0xF0F0);
        assert_eq!(registers.get_bc(), 0xF0F0);
        registers.set_bc(0xF00F);
        assert_eq!(registers.get_b(), 0xF0);
        assert_eq!(registers.get_c(), 0x0F);

        registers.set_de(0xF0F0);
        assert_eq!(registers.get_de(), 0xF0F0);
        registers.set_de(0xF00F);
        assert_eq!(registers.get_d(), 0xF0);
        assert_eq!(registers.get_e(), 0x0F);

        registers.set_hl(0xF0F0);
        assert_eq!(registers.get_hl(), 0xF0F0);
        registers.set_hl(0xF00F);
        assert_eq!(registers.get_h(), 0xF0);
        assert_eq!(registers.get_l(), 0x0F);
    }
}