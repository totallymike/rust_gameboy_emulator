use bitflags::bitflags;

bitflags! {
    pub struct Flags: u8 {
        const ZERO = 0b10000000;
        const SUBTRACT = 0b01000000;
        const HALF_CARRY = 0b00100000;
        const CARRY = 0b00010000;
    }
}

impl Flags {
    pub fn zero(&self) -> bool {
        self.contains(Flags::ZERO)
    }

    pub fn subtract(&self) -> bool {
        self.contains(Flags::SUBTRACT)
    }

    pub fn half_carry(&self) -> bool {
        self.contains(Flags::HALF_CARRY)
    }

    pub fn carry(&self) -> bool {
        self.contains(Flags::CARRY)
    }

    pub(in super) fn set_zero(&mut self, value: bool) {
        self.set(Flags::ZERO, value);
    }

    pub(in super) fn set_subtract(&mut self, value: bool) {
        self.set(Flags::SUBTRACT, value);
    }

    pub(in super) fn set_half_carry(&mut self, value: bool) {
        self.set(Flags::HALF_CARRY, value);
    }

    pub(in super) fn set_carry(&mut self, value: bool) {
        self.set(Flags::CARRY, value);
    }
}

impl std::convert::From<Flags> for u8 {
    fn from(flags: Flags) -> u8 {
        flags.bits
    }
}

impl std::convert::From<u8> for Flags {
    fn from(byte: u8) -> Self {
        Flags::from_bits_truncate(byte)
    }
}

#[cfg(test)]
mod tests {
    use super::Flags;

    #[test]
    fn play_with_flags() {
        let flags = Flags::ZERO | Flags::CARRY;
        assert!(flags.zero());
        assert!(flags.carry());
        assert!(!flags.subtract());
        assert!(!flags.half_carry());
    }

    #[test]
    fn flags_from_registers() {
        use super::super::registers::Registers;
        let registers = Registers::init();
        let flags = registers.flags();
        assert!(flags.zero());
        assert!(!flags.subtract());
        assert!(flags.carry());
        assert!(flags.half_carry());
    }
}