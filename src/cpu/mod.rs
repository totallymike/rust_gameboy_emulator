pub mod flags;
pub mod instructions;
pub mod registers;

#[cfg(test)]
mod tests;

use self::flags::Flags;
use self::instructions::{
    ArithmeticTarget, Arithmetic16Target, BitPosition, IncDecTarget, IncDec16Target, Instruction, JumpTest, LoadByteSource,
    LoadByteTarget, LoadHighByteTarget, LoadIndirectByteTarget, LoadType, LoadWordSource, LoadWordTarget, OpCode,
    RstTarget, StackTarget,
};
use self::registers::Registers;
use crate::memory_bus::MemoryBus;

/// Used to mask values to detect half-carries
pub const HALF_CARRY_MASK: u8 = 0x0F;

/// A convevience to exhaustively match one and zero without using booleans
enum BitValue {
    ZERO,
    ONE,
}

pub struct CPU {
    registers: Registers,
    pc: u16,
    sp: u16,
    bus: MemoryBus,
    /// Used to denote a halted state.
    is_halted: bool,
    /// IME (Interrupt Master Enable) flag.  
    /// Set HIGH for Interrupts enabled; LOW for disabled.
    interrupt_enabled: bool,
}

impl CPU {
    pub fn new() -> CPU {
        CPU {
            registers: Registers::new(),
            pc: 0x0,
            sp: 0x0,
            bus: MemoryBus::new(),
            is_halted: false,
            interrupt_enabled: true,
        }
    }

    pub fn init() -> CPU {
        println!("{:?}", self::instructions::NON_PREFIXED_OPCODES[0x80]);
        CPU {
            registers: Registers::init(),
            pc: 0x100,
            sp: 0xFFFE,
            bus: MemoryBus::new(),
            is_halted: false,
            interrupt_enabled: true,
        }
    }

    pub fn read_next_byte(&self) -> u8 {
        self.bus.read_byte(self.pc + 1)
    }

    pub fn read_next_word(&self) -> u16 {
        self.bus.read_word(self.pc + 1)
    }

    pub fn step(&mut self) {
        let mut instruction_byte = self.bus.read_byte(self.pc);
        let prefixed = instruction_byte == 0xCB;
        if prefixed {
            instruction_byte = self.bus.read_byte(self.pc + 1);
        }

        let next_pc = if let Some(instruction) = OpCode::from_byte(instruction_byte, prefixed) {
            self.execute(instruction)
        } else {
            let description = format!(
                "0x{}{:x}",
                if prefixed { "CB" } else { "" },
                instruction_byte
            );
            panic!("Unknown byte found for: {}", description);
        };

        self.pc = next_pc;
    }

    pub fn execute(&mut self, opcode: OpCode) -> u16 {
        // Do nothing if the CPU is halted.
        if self.is_halted {
            return self.pc;
        }

        let step_distance = opcode.size as u16;
        match opcode.instruction {
            Instruction::NOP => {
                self.pc.wrapping_add(step_distance)
            }
            Instruction::PREFIX => {
                unreachable!("This will never happen");
            }
            Instruction::UNSUPPORTED => {
                panic!("Unsupported opcode: {:?}", opcode);
            }
            Instruction::STOP => {
                panic!("Somebody hit the stop signal!");
            }
            Instruction::HALT => {
                self.is_halted = true;
                self.pc.wrapping_add(step_distance)
            }
            Instruction::INC(target) => {
                self.inc(target);
                self.pc.wrapping_add(step_distance)
            }
            Instruction::DEC(target) => {
                self.dec(target);
                self.pc.wrapping_add(step_distance)
            }
            Instruction::INC16(target) => {
                self.inc16(target);
                self.pc.wrapping_add(step_distance)
            }
            Instruction::DEC16(target) => {
                self.dec16(target);
                self.pc.wrapping_add(step_distance)
            }
            Instruction::CCF => {
                self.ccf();
                self.pc.wrapping_add(step_distance)
            }
            Instruction::SCF => {
                self.scf();
                self.pc.wrapping_add(step_distance)
            }
            Instruction::RL(target) => {
                self.rl(target);
                self.pc.wrapping_add(step_distance)
            }
            Instruction::RLC(target) => {
                self.rlc(target);
                self.pc.wrapping_add(step_distance)
            }
            Instruction::RLCA => {
                self.rlca();
                self.pc.wrapping_add(step_distance)
            }
            Instruction::RLA => {
                self.rla();
                self.pc.wrapping_add(step_distance)
            }
            Instruction::RR(target) => {
                self.rr(target);
                self.pc.wrapping_add(step_distance)
            }
            Instruction::RRC(target) => {
                self.rrc(target);
                self.pc.wrapping_add(step_distance)
            }
            Instruction::RRCA => {
                self.rrca();
                self.pc.wrapping_add(step_distance)
            }
            Instruction::RRA => {
                self.rra();
                self.pc.wrapping_add(step_distance)
            }
            Instruction::SLA(target) => {
                self.shift_left_arithmetic(target);
                self.pc.wrapping_add(step_distance)
            }
            Instruction::SRA(target) => {
                self.shift_right_arithmetic(target);
                self.pc.wrapping_add(step_distance)
            }
            Instruction::SWAP(target) => {
                self.swap(target);
                self.pc.wrapping_add(step_distance)
            }
            Instruction::SRL(target) => {
                self.shift_right_logic(target);
                self.pc.wrapping_add(step_distance)
            }
            Instruction::CPL => {
                self.cpl();
                self.pc.wrapping_add(step_distance)
            }
            Instruction::BIT(position, target) => {
                let target_value: u8 = match target {
                    LoadByteTarget::A => self.registers.a,
                    LoadByteTarget::B => self.registers.b,
                    LoadByteTarget::C => self.registers.c,
                    LoadByteTarget::D => self.registers.d,
                    LoadByteTarget::E => self.registers.e,
                    LoadByteTarget::H => self.registers.h,
                    LoadByteTarget::L => self.registers.l,
                    LoadByteTarget::HLI => self.bus.read_byte(self.registers.get_hl()),
                };
                let value = target_value >> (position as u8);
                // Set up a new flags register with HC:1, and preserve the existing C value
                let mut new_flags = Flags::HALF_CARRY | (self.flags() & Flags::CARRY);
                if value & 0x01 == 0 {
                    new_flags |= Flags::ZERO;
                }
                self.registers.f = new_flags;
                self.pc.wrapping_add(step_distance)
            }
            Instruction::DI => {
                self.interrupt_enabled = false;
                self.pc.wrapping_add(step_distance)
            }
            Instruction::EI => {
                self.interrupt_enabled = true;
                self.pc.wrapping_add(step_distance)
            }
            Instruction::RESET(position, target) => {
                self.set_bit(target, position, BitValue::ZERO);
                self.pc.wrapping_add(step_distance)
            }
            Instruction::SET(position, target) => {
                self.set_bit(target, position, BitValue::ONE);
                self.pc.wrapping_add(step_distance)
            }
            Instruction::LD(LoadType::Word(target, source)) => {
                let value: u16 = match source {
                    LoadWordSource::D16 => self.read_next_word(),
                    LoadWordSource::SP => self.sp,
                };
                match target {
                    LoadWordTarget::BC => self.registers.set_bc(value),
                    LoadWordTarget::DE => self.registers.set_de(value),
                    LoadWordTarget::HL => self.registers.set_hl(value),
                    LoadWordTarget::SP => self.sp = value,
                    LoadWordTarget::I16 => {
                        let address = self.read_next_word();
                        self.bus.write_word(address, value);
                    }
                };
                self.pc.wrapping_add(step_distance)
            }
            Instruction::LD(LoadType::Byte(target, source)) => {
                let value = match source {
                    LoadByteSource::A => self.registers.a,
                    LoadByteSource::B => self.registers.b,
                    LoadByteSource::C => self.registers.c,
                    LoadByteSource::D => self.registers.d,
                    LoadByteSource::E => self.registers.e,
                    LoadByteSource::H => self.registers.h,
                    LoadByteSource::L => self.registers.l,
                    LoadByteSource::D8 => self.bus.read_byte(self.pc + 1),
                    LoadByteSource::HLI => self.bus.read_byte(self.registers.get_hl()),
                };
                match target {
                    LoadByteTarget::A => self.registers.a = value,
                    LoadByteTarget::B => self.registers.b = value,
                    LoadByteTarget::C => self.registers.c = value,
                    LoadByteTarget::D => self.registers.d = value,
                    LoadByteTarget::E => self.registers.e = value,
                    LoadByteTarget::H => self.registers.h = value,
                    LoadByteTarget::L => self.registers.l = value,
                    LoadByteTarget::HLI => self.bus.write_byte(self.registers.get_hl(), value),
                };
                self.pc.wrapping_add(step_distance)
            }
            Instruction::LD(LoadType::IndirectByteFromA(target)) => {
                use LoadIndirectByteTarget::*;
                let value = self.registers.a;
                let address = match target {
                    A16 => self.read_next_word(),
                    BC => self.registers.get_bc(),
                    DE => self.registers.get_de(),
                    HlInc => {
                        let target = self.registers.get_hl();
                        self.registers.set_hl(target.wrapping_add(1));
                        target
                    }
                    HlDec => {
                        let target = self.registers.get_hl();
                        self.registers.set_hl(target.wrapping_sub(1));
                        target
                    }
                };
                self.bus.write_byte(address, value);
                self.pc.wrapping_add(step_distance)
            }
            Instruction::LD(LoadType::IndirectByteToA(target)) => {
                use LoadIndirectByteTarget::*;
                let address = match target {
                    A16 => self.read_next_word(),
                    BC => self.registers.get_bc(),
                    DE => self.registers.get_de(),
                    HlInc => {
                        let target = self.registers.get_hl();
                        self.registers.set_hl(target.wrapping_add(1));
                        target
                    }
                    HlDec => {
                        let target = self.registers.get_hl();
                        self.registers.set_hl(target.wrapping_sub(1));
                        target
                    }
                };
                let value = self.bus.read_byte(address);
                self.registers.a = value;
                self.pc.wrapping_add(step_distance)
            }
            Instruction::LD(LoadType::HighFromA(target)) => {
                use LoadHighByteTarget::*;
                let value = self.registers.a;
                let load_target: u16 = 0xFF00 | match target {
                    C => self.registers.c,
                    D8 => self.read_next_byte(),
                } as u16;
                self.bus.write_byte(load_target, value);
                self.pc.wrapping_add(step_distance)
            }
            Instruction::LD(LoadType::HighToA(source)) => {
                use LoadHighByteTarget::*;
                let address: u16 = 0xFF00 | match source {
                    C => self.registers.c,
                    D8 => self.read_next_byte(),
                } as u16;
                self.registers.a = self.bus.read_byte(address);
                self.pc.wrapping_add(step_distance)
            }
            Instruction::LD(LoadType::HlSpE) => {
                let value = (self.read_next_byte() as i8) as u16;
                let sp = self.sp;
                let (new_sp, did_overflow) = self.sp.overflowing_add(value);
                let mut new_flags = Flags::empty();
                if (value & 0x0FFF) | (sp & 0x0FFF) > 0x0FFF {
                    new_flags |= Flags::HALF_CARRY;
                }
                if did_overflow {
                    new_flags |= Flags::CARRY;
                }
                self.registers.set_hl(new_sp);
                self.registers.f = new_flags;
                self.pc.wrapping_add(step_distance)
            }
            Instruction::LD(LoadType::SpHl) => {
                self.sp = self.registers.get_hl();
                self.pc.wrapping_add(step_distance)
            }
            Instruction::JpHl => {
                self.registers.get_hl()
            }
            Instruction::JP(test) => {
                let jump_condition = match test {
                    JumpTest::NotCarry => !self.registers.f.carry(),
                    JumpTest::Carry => self.registers.f.carry(),
                    JumpTest::NotZero => !self.registers.f.zero(),
                    JumpTest::Zero => self.registers.f.zero(),
                    JumpTest::Always => true,
                };
                self.jump(jump_condition)
            }
            Instruction::JR(test) => {
                let jump_condition = match test {
                    JumpTest::NotCarry => !self.registers.f.carry(),
                    JumpTest::Carry => self.registers.f.carry(),
                    JumpTest::NotZero => !self.registers.f.zero(),
                    JumpTest::Zero => self.registers.f.zero(),
                    JumpTest::Always => true,
                };
                self.jump_relative(jump_condition)
            }
            Instruction::RST(target) => {
                use RstTarget::*;
                let jump_target: u16 = match target {
                    X00 => 0x0000,
                    X10 => 0x0010,
                    X20 => 0x0020,
                    X30 => 0x0030,
                    X08 => 0x0008,
                    X18 => 0x0018,
                    X28 => 0x0028,
                    X38 => 0x0038,
                };
                let next_pc = self.pc.wrapping_add(1);
                self.push(next_pc);
                jump_target
            }
            Instruction::CALL(test) => {
                let jump_condition = match test {
                    JumpTest::NotCarry => !self.registers.f.carry(),
                    JumpTest::Carry => self.registers.f.carry(),
                    JumpTest::NotZero => !self.registers.f.zero(),
                    JumpTest::Zero => self.registers.f.zero(),
                    JumpTest::Always => true,
                };
                self.call(jump_condition)
            }
            Instruction::RET(test) => {
                let jump_condition = match test {
                    JumpTest::NotCarry => !self.registers.f.carry(),
                    JumpTest::Carry => self.registers.f.carry(),
                    JumpTest::NotZero => !self.registers.f.zero(),
                    JumpTest::Zero => self.registers.f.zero(),
                    JumpTest::Always => true,
                };
                self.return_(jump_condition)
            }
            Instruction::RETI => {
                self.interrupt_enabled = true;
                self.return_(true)
            }
            Instruction::ADD(target) => {
                let value = self.read_arithmetic_target(target);
                let new_value = self.add_without_carry(value);
                self.registers.a = new_value;
                self.pc.wrapping_add(step_distance)
            }
            Instruction::AddHl(target) => {
                use Arithmetic16Target::*;
                let value = match target {
                    BC => self.registers.get_bc(),
                    DE => self.registers.get_de(),
                    HL => self.registers.get_hl(),
                    SP => self.sp,
                };
                let old_value = self.registers.get_hl();
                let new_value = self.add_16(old_value, value);
                self.registers.set_hl(new_value);
                self.pc.wrapping_add(step_distance)
            }
            Instruction::AddSp => {
                let value: u16 = (self.read_next_byte() as i8) as u16;
                self.sp = self.sp.wrapping_add(value);
                self.pc.wrapping_add(step_distance)
            }
            Instruction::ADC(target) => {
                let value = self.read_arithmetic_target(target);
                let new_value = self.add_with_carry(value);
                self.registers.a = new_value;
                self.pc.wrapping_add(step_distance)
            }
            Instruction::SUB(target) => {
                let value = self.read_arithmetic_target(target);
                let new_value = self.sub_without_carry(value);
                self.registers.a = new_value;
                self.pc.wrapping_add(step_distance)
            }
            Instruction::SBC(target) => {
                let value = self.read_arithmetic_target(target);
                let new_value = self.sub_with_carry(value);
                self.registers.a = new_value;
                self.pc.wrapping_add(step_distance)
            }
            Instruction::DAA => {
                let mut value = self.registers.a;
                let flags = self.flags();
                value = if flags.contains(Flags::SUBTRACT) {
                    let mut new_value = value;
                    if flags.carry() {
                        new_value += 0x60;
                    }
                    if flags.half_carry() {
                        new_value += 0x06;
                    }
                    new_value
                } else {
                    let mut new_value = value;
                    if flags.contains(Flags::CARRY) || (value & 0xFF) > 0x99 {
                        self.registers.f.set_carry(true);
                        new_value += 0x60;
                    }
                    if flags.contains(Flags::HALF_CARRY) || (value & 0x0F) > 0x09 {
                        new_value += 0x06;
                    }
                    new_value
                };
                self.registers.a = value;
                if value == 0 {
                    self.registers.f |= Flags::ZERO;
                    self.registers.f.set_half_carry(false);
                }
                self.pc.wrapping_add(1)
            }
            Instruction::AND(target) => {
                let value = self.read_arithmetic_target(target);
                let new_value = self.and(value);
                self.registers.a = new_value;
                self.pc.wrapping_add(step_distance)
            }
            Instruction::XOR(target) => {
                let value = self.read_arithmetic_target(target);
                let new_value = self.xor(value);
                self.registers.a = new_value;
                self.pc.wrapping_add(step_distance)
            }
            Instruction::OR(target) => {
                let value = self.read_arithmetic_target(target);
                let new_value = self.or(value);
                self.registers.a = new_value;
                self.pc.wrapping_add(step_distance)
            }
            Instruction::CP(target) => {
                let value = self.read_arithmetic_target(target);
                self.sub_without_carry(value);
                self.pc.wrapping_add(step_distance)
            }
            Instruction::PUSH(target) => {
                let value = match target {
                    StackTarget::BC => self.registers.get_bc(),
                    StackTarget::DE => self.registers.get_de(),
                    StackTarget::HL => self.registers.get_hl(),
                    StackTarget::AF => self.registers.get_af(),
                };
                self.push(value);
                self.pc.wrapping_add(step_distance)
            }
            Instruction::POP(target) => {
                let value = self.pop();
                match target {
                    StackTarget::BC => self.registers.set_bc(value),
                    StackTarget::DE => self.registers.set_de(value),
                    StackTarget::HL => self.registers.set_hl(value),
                    StackTarget::AF => self.registers.set_af(value),
                };
                self.pc.wrapping_add(step_distance)
            }
        }
    }

    fn inc(&mut self, target: IncDecTarget) {
        use IncDecTarget::*;
        let destination: &mut u8 = match target {
            A => &mut self.registers.a,
            B => &mut self.registers.b,
            C => &mut self.registers.c,
            D => &mut self.registers.d,
            E => &mut self.registers.e,
            H => &mut self.registers.h,
            L => &mut self.registers.l,
            HLI => {
                let address = self.registers.get_hl();
                self.bus.handle_to_address(address)
            }
        };
        let previous_value = *destination;
        *destination = destination.wrapping_add(1);
        let mut new_flags = match *destination {
            0 => Flags::ZERO,
            _ => Flags::empty(),
        };

        if (previous_value & HALF_CARRY_MASK) > (*destination & HALF_CARRY_MASK) {
            new_flags |= Flags::HALF_CARRY;
        }
        self.registers.f = new_flags;
    }

    fn inc16(&mut self, target: IncDec16Target) {
        use IncDec16Target::*;
        let value = match target {
            BC => self.registers.get_bc(),
            DE => self.registers.get_de(),
            HL => self.registers.get_hl(),
            SP => self.sp,
        }.wrapping_add(1);

        match target {
            BC => self.registers.set_bc(value),
            DE => self.registers.set_de(value),
            HL => self.registers.set_hl(value),
            SP => { self.sp = value },
        };
    }

    fn dec16(&mut self, target: IncDec16Target) {
        use IncDec16Target::*;
        let value = match target {
            BC => self.registers.get_bc(),
            DE => self.registers.get_de(),
            HL => self.registers.get_hl(),
            SP => self.sp,
        }.wrapping_sub(1);

        match target {
            BC => self.registers.set_bc(value),
            DE => self.registers.set_de(value),
            HL => self.registers.set_hl(value),
            SP => { self.sp = value },
        };
    }

    fn dec(&mut self, target: IncDecTarget) {
        use IncDecTarget::*;
        let destination: &mut u8 = match target {
            A => &mut self.registers.a,
            B => &mut self.registers.b,
            C => &mut self.registers.c,
            D => &mut self.registers.d,
            E => &mut self.registers.e,
            H => &mut self.registers.h,
            L => &mut self.registers.l,
            HLI => {
                let address = self.registers.get_hl();
                self.bus.handle_to_address(address)
            }
        };
        let previous_value = *destination;
        *destination = destination.wrapping_sub(1);
        let mut new_flags = Flags::SUBTRACT;
        if *destination == 0 {
            new_flags |= Flags::ZERO;
        }

        if (previous_value & HALF_CARRY_MASK) < (*destination & HALF_CARRY_MASK) {
            new_flags |= Flags::HALF_CARRY;
        }

        self.registers.f = new_flags;
    }

    fn ccf(&mut self) {
        self.registers.f.set_half_carry(false);
        self.registers.f.set_subtract(false);
        self.registers.f.toggle(Flags::CARRY);
    }

    fn scf(&mut self) {
        self.registers.f.set_half_carry(false);
        self.registers.f.set_subtract(false);
        self.registers.f.set_carry(true);
    }

    fn rotate_left(&mut self, target: LoadByteTarget, check_zero: bool) {
        let old_value = self.read_byte_target(target);
        let should_carry = old_value & 0x80;
        let new_value = old_value.rotate_left(1);
        self.write_byte_target(target, new_value);

        let mut new_flags = Flags::empty();
        if check_zero && new_value == 0 {
            new_flags |= Flags::ZERO;
        }
        if should_carry != 0 {
            new_flags |= Flags::CARRY;
        }
        self.registers.f = new_flags;
    }

    fn rotate_right(&mut self, target: LoadByteTarget, check_zero: bool) {
        let old_value = self.read_byte_target(target);
        let should_carry = old_value & 0x01;
        let new_value = old_value.rotate_right(1);
        self.write_byte_target(target, new_value);

        let mut new_flags = Flags::empty();
        if check_zero && new_value == 0 {
            new_flags |= Flags::ZERO;
        }
        if should_carry != 0 {
            new_flags |= Flags::CARRY;
        }
        self.registers.f = new_flags;
    }

    fn rotate_left_through_carry(&mut self, target: LoadByteTarget, check_zero: bool) {
        let old_value = self.read_byte_target(target);
        let mut new_flags = match old_value & 0x80 {
            0x80 => Flags::CARRY,
            _ => Flags::empty(),
        };
        let new_bit_0: u8 = match self.flags().carry() {
            true => 1,
            false => 0,
        };
        let new_value = (old_value << 1) | new_bit_0;

        if check_zero && new_value == 0 {
            new_flags |= Flags::ZERO;
        }

        self.write_byte_target(target, new_value);
        self.registers.f = new_flags;
    }

    fn rotate_right_through_carry(&mut self, target: LoadByteTarget, check_zero: bool) {
        let old_value = self.read_byte_target(target);
        let mut new_flags = match old_value & 0x01 {
            0x01 => Flags::CARRY,
            _ => Flags::empty(),
        };
        let new_bit_7: u8 = match self.flags().carry() {
            true => 0x80,
            false => 0x00,
        };
        let new_value = (old_value >> 1) | new_bit_7;

        if check_zero && new_value == 0 {
            new_flags |= Flags::ZERO;
        }

        self.write_byte_target(target, new_value);
        self.registers.f = new_flags;
    }

    fn rl(&mut self, target: LoadByteTarget) {
        self.rotate_left_through_carry(target, true);
    }

    fn rlc(&mut self, target: LoadByteTarget) {
        self.rotate_left(target, true);
    }

    fn rlca(&mut self) {
        self.rotate_left(LoadByteTarget::A, false);
    }

    fn rr(&mut self, target: LoadByteTarget) {
        self.rotate_right_through_carry(target, true);
    }

    fn rrc(&mut self, target: LoadByteTarget) {
        self.rotate_right(target, true);
    }

    fn rrca(&mut self) {
        self.rotate_right(LoadByteTarget::A, false);
    }

    fn rla(&mut self) {
        self.rotate_left_through_carry(LoadByteTarget::A, false);
    }

    fn rra(&mut self) {
        self.rotate_right_through_carry(LoadByteTarget::A, false);
    }

    fn swap(&mut self, target: LoadByteTarget) {
        let old_value = self.read_byte_target(target);
        let new_upper_half = (old_value & 0x0F) << 4;
        let new_lower_half = old_value >> 4;
        let new_value = new_upper_half | new_lower_half;
        self.write_byte_target(target, new_value);
        self.registers.f = if new_value == 0 {
            Flags::ZERO
        } else {
            Flags::empty()
        };
    }

    fn cpl(&mut self) {
        self.registers.a ^= 0xFF;
        self.registers.f |= Flags::HALF_CARRY | Flags::SUBTRACT;
    }

    fn set_bit(&mut self, target: LoadByteTarget, position: BitPosition, value: BitValue) {
        let old_value = self.read_byte_target(target);
        let mask = 0x01 << (position as u8);
        let new_value = match value {
            BitValue::ONE => (old_value | mask),
            BitValue::ZERO => (old_value ^ mask),
        };
        self.write_byte_target(target, new_value);
    }

    fn shift_right_logic(&mut self, target: LoadByteTarget) {
        self.shift_right(target, false);
    }

    fn shift_right_arithmetic(&mut self, target: LoadByteTarget) {
        self.shift_right(target, true);
    }

    fn shift_right(&mut self, target: LoadByteTarget, preserve_bit_7: bool) {
        let old_value = self.read_byte_target(target);

        // Capture bit seven to preserve it for later.
        let did_carry = old_value & 0x01 == 0x01;
        let mut new_flags = Flags::empty();
        let new_value = {
            let high_bit = old_value & 0x80;
            let value = old_value >> 1;
            if preserve_bit_7 {
                value | high_bit
            } else {
                value
            }
        };

        self.write_byte_target(target, new_value);

        if did_carry {
            new_flags |= Flags::CARRY;
        }
        if new_value == 0 {
            new_flags |= Flags::ZERO;
        }

        self.registers.f = new_flags;
    }

    fn shift_left_arithmetic(&mut self, target: LoadByteTarget) {
        let old_value = self.read_byte_target(target);
        let did_carry = old_value & 0x80 != 0;
        let new_value = old_value << 1;

        self.write_byte_target(target, new_value);

        let mut new_flags = Flags::empty();
        if did_carry {
            new_flags |= Flags::CARRY;
        }
        if new_value == 0 {
            new_flags |= Flags::ZERO;
        }
        self.registers.f = new_flags;
    }

    fn jump(&self, should_jump: bool) -> u16 {
        if should_jump {
            let least_significant_byte = self.bus.read_byte(self.pc + 1) as u16;
            let most_significant_byte = self.bus.read_byte(self.pc + 2) as u16;
            (most_significant_byte << 8) | least_significant_byte
        } else {
            self.pc.wrapping_add(3)
        }
    }

    fn jump_relative(&self, should_jump: bool) -> u16 {
        if should_jump {
            let distance = self.read_next_byte() as i8;
            self.pc.wrapping_add(distance as u16)
        } else {
            self.pc.wrapping_add(2)
        }
    }

    fn call(&mut self, should_jump: bool) -> u16 {
        let next_pc = self.pc.wrapping_add(3);
        if should_jump {
            self.push(next_pc);
            self.read_next_word()
        } else {
            next_pc
        }
    }

    fn return_(&mut self, should_jump: bool) -> u16 {
        if should_jump {
            self.pop()
        } else {
            self.pc.wrapping_add(1)
        }
    }

    fn add_with_carry(&mut self, value: u8) -> u8 {
        self.add(value, true)
    }

    fn add_without_carry(&mut self, value: u8) -> u8 {
        self.add(value, false)
    }

    fn add(&mut self, value: u8, with_carry: bool) -> u8 {
        let additional_carry = if with_carry && self.registers.f.carry() {
            1
        } else {
            0
        };
        let (new_value, did_overflow) = self.registers.a.overflowing_add(value);
        let (new_value, did_overflow2) = new_value.overflowing_add(additional_carry);
        let mut new_flags = Flags::empty();

        if new_value == 0 {
            new_flags.set_zero(true);
        }

        new_flags.set(Flags::CARRY, did_overflow || did_overflow2);

        if (self.registers.a & HALF_CARRY_MASK) + (value & HALF_CARRY_MASK) > 0xF {
            new_flags = new_flags | Flags::HALF_CARRY;
        }
        self.registers.f = new_flags;
        return new_value;
    }

    fn add_16(&mut self, a: u16, b: u16) -> u16 {
        let (new_value, did_carry) = a.overflowing_add(b);

        // Preserve the original value of Z, the only flag not clobbered.
        let mut new_flags = self.flags() & Flags::ZERO;

        if (a & 0xFFF) + (b & 0xFFF) > 0xFF {
            new_flags |= Flags::HALF_CARRY;
        }

        if did_carry {
            new_flags |= Flags::CARRY;
        }
        self.registers.f = new_flags;

        new_value
    }

    fn sub_without_carry(&mut self, value: u8) -> u8 {
        self.sub(value, false)
    }

    fn sub_with_carry(&mut self, value: u8) -> u8 {
        self.sub(value, true)
    }

    fn sub(&mut self, value: u8, with_carry: bool) -> u8 {
        let additional_carry = if with_carry && self.registers.f.carry() {
            1
        } else {
            0
        };

        let (new_value, did_carry) = self.registers.a.overflowing_sub(value);
        let (new_value, did_carry2) = new_value.overflowing_sub(additional_carry);

        let mut new_flags = Flags::SUBTRACT;

        // If subtracting the lower nibble of the value from the lower nibble of A
        // would result in an underflow, then we've got a half carry.
        if (self.registers.a & HALF_CARRY_MASK) < (value & HALF_CARRY_MASK) + additional_carry {
            new_flags |= Flags::HALF_CARRY;
        }

        if new_value == 0 {
            new_flags |= Flags::ZERO;
        }

        if did_carry || did_carry2 {
            new_flags |= Flags::CARRY;
        }

        self.registers.f = new_flags;
        new_value
    }

    fn and(&mut self, value: u8) -> u8 {
        let new_value = self.registers.a & value;
        let mut new_flags = Flags::HALF_CARRY;
        if new_value == 0 {
            new_flags |= Flags::ZERO;
        }
        self.registers.f = new_flags;
        new_value
    }

    fn xor(&mut self, value: u8) -> u8 {
        let new_value = self.registers.a ^ value;
        self.registers.f = match new_value {
            0 => Flags::ZERO,
            _ => Flags::empty(),
        };
        new_value
    }

    fn or(&mut self, value: u8) -> u8 {
        let new_value = self.registers.a | value;
        self.registers.f = match new_value {
            0 => Flags::ZERO,
            _ => Flags::empty(),
        };
        new_value
    }

    fn push(&mut self, value: u16) {
        self.sp = self.sp.wrapping_sub(2);
        self.bus.write_word(self.sp, value);
    }

    fn pop(&mut self) -> u16 {
        let value = self.bus.read_word(self.sp);
        self.sp = self.sp.wrapping_add(2);
        value
    }

    fn flags(&self) -> Flags {
        self.registers.f
    }

    fn read_arithmetic_target(&self, target: ArithmeticTarget) -> u8 {
        match target {
            ArithmeticTarget::A => self.registers.a,
            ArithmeticTarget::B => self.registers.b,
            ArithmeticTarget::C => self.registers.c,
            ArithmeticTarget::D => self.registers.d,
            ArithmeticTarget::E => self.registers.e,
            ArithmeticTarget::H => self.registers.h,
            ArithmeticTarget::L => self.registers.l,
            ArithmeticTarget::D8 => self.read_next_byte(),
            ArithmeticTarget::HLI => self.bus.read_byte(self.registers.get_hl()),
        }
    }

    fn read_byte_target(&self, target: LoadByteTarget) -> u8 {
        use LoadByteTarget::*;

        match target {
            A => self.registers.a,
            B => self.registers.b,
            C => self.registers.c,
            D => self.registers.d,
            E => self.registers.e,
            H => self.registers.h,
            L => self.registers.l,
            HLI => {
                let address = self.registers.get_hl();
                self.bus.read_byte(address)
            }
        }
    }

    fn write_byte_target(&mut self, target: LoadByteTarget, value: u8) {
        use LoadByteTarget::*;

        let destination: &mut u8 = match target {
            A => &mut self.registers.a,
            B => &mut self.registers.b,
            C => &mut self.registers.c,
            D => &mut self.registers.d,
            E => &mut self.registers.e,
            H => &mut self.registers.h,
            L => &mut self.registers.l,
            HLI => {
                let address = self.registers.get_hl();
                self.bus.handle_to_address(address)
            }
        };

        *destination = value;
    }
}
