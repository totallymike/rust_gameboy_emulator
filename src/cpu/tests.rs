use super::flags::Flags;
use super::instructions::Instruction::*;
use super::instructions::{
    ArithmeticTarget, Arithmetic16Target, BitPosition, IncDec16Target, IncDecTarget, Instruction, JumpTest,
    LoadByteSource, LoadByteTarget, LoadIndirectByteTarget, LoadHighByteTarget, LoadType, LoadWordSource,
    LoadWordTarget, OpCode, RstTarget, StackTarget,
};
use super::CPU;

fn write_to_pci(cpu: &mut CPU, instruction: Instruction) {
    let opcode = OpCode::from_instruction(instruction);
    cpu.bus.write_byte(cpu.pc, opcode.number);
}
fn write_prefix_instruction(cpu: &mut CPU, instruction: Instruction) {
    let opcode = OpCode::from_instruction(instruction);
    cpu.bus.write_byte(cpu.pc, 0xCB);
    cpu.bus.write_byte(cpu.pc + 1, opcode.number);
}

fn write_instruction_to_pci_offset(cpu: &mut CPU, offset: u16, instruction: Instruction) {
    let opcode = OpCode::from_instruction(instruction);
    cpu.bus.write_byte(cpu.pc + offset, opcode.number);
}

fn write_to_pci_offset(cpu: &mut CPU, offset: u16, byte: u8) {
    cpu.bus.write_byte(cpu.pc + offset, byte);
}

fn write_byte(address: u16, cpu: &mut CPU, byte: u8) {
    cpu.bus.write_byte(address, byte);
}

#[test]
fn all_opcodes_are_present() {
    let unused_instructions = super::instructions::NON_PREFIXED_OPCODES
        .iter()
        .enumerate()
        .filter_map(|(idx, &maybe_opcode)| {
            if maybe_opcode.is_none() {
                Some(format!("0x{:X}", idx))
            } else {
                None
            }
        })
        .collect::<Vec<String>>()
        .join(", ");
    assert_eq!(
        super::instructions::NON_PREFIXED_OPCODES
            .iter()
            .filter_map(|&opcode| opcode)
            .count(),
        256,
        "{:?} are unused",
        unused_instructions
    );
}

#[test]
fn cpu_init() {
    let cpu = CPU::init();
    let registers = &cpu.registers;
    assert_eq!(registers.get_af(), 0x01B0u16);
}

#[test]
fn halt() {
    let mut cpu = CPU::init();
    write_to_pci(&mut cpu, Instruction::HALT);
    cpu.step();
    assert_eq!(cpu.is_halted, true);
    // Also prove that subsequent steps don't advance the PC.
    assert_eq!(cpu.pc, 0x101);
    cpu.step();
    cpu.step();
    assert_eq!(cpu.pc, 0x101);
}

#[test]
fn inc() {
    let mut cpu = CPU::init();
    cpu.registers.c = 0x00;
    write_to_pci(&mut cpu, Instruction::INC(IncDecTarget::C));
    cpu.step();
    assert_eq!(cpu.registers.c, 0x01);
    assert_eq!(cpu.registers.f, Flags::empty());
    cpu.registers.d = 0xFF;
    write_to_pci(&mut cpu, Instruction::INC(IncDecTarget::D));
    cpu.step();
    assert_eq!(cpu.registers.d, 0x00);
    assert_eq!(cpu.registers.f, Flags::ZERO | Flags::HALF_CARRY);
    cpu.registers.e = 0x0F;
    write_to_pci(&mut cpu, Instruction::INC(IncDecTarget::E));
    cpu.step();
    assert_eq!(cpu.registers.e, 0x10);
    assert_eq!(cpu.registers.f, Flags::HALF_CARRY);
    cpu.bus.write_byte(0x1000, 0x01);
    cpu.registers.set_hl(0x1000);
    write_to_pci(&mut cpu, Instruction::INC(IncDecTarget::HLI));
    cpu.step();
    assert_eq!(cpu.bus.read_word(0x1000), 0x02);
    assert_eq!(cpu.registers.f, Flags::empty());
}

#[test]
fn inc16() {
    let mut cpu = CPU::init();
    cpu.registers.set_bc(0x00FF);
    let old_flags = cpu.flags();
    write_to_pci(&mut cpu, Instruction::INC16(IncDec16Target::BC));
    cpu.step();
    // Flags aren't affected
    assert_eq!(cpu.flags(), old_flags);
    assert_eq!(cpu.registers.get_bc(), 0x0100);
}

#[test]
fn dec16() {
    let mut cpu = CPU::init();
    cpu.registers.set_bc(0x0100);
    write_to_pci(&mut cpu, Instruction::DEC16(IncDec16Target::BC));
    cpu.step();
    assert_eq!(cpu.registers.get_bc(), 0x00FF);
}

#[test]
fn dec() {
    let mut cpu = CPU::init();
    cpu.registers.c = 0x00;
    write_to_pci(&mut cpu, Instruction::DEC(IncDecTarget::C));
    cpu.step();
    assert_eq!(cpu.registers.c, 0xFF);
    assert_eq!(cpu.registers.f, Flags::SUBTRACT | Flags::HALF_CARRY);
    cpu.registers.d = 0xFF;
    write_to_pci(&mut cpu, Instruction::DEC(IncDecTarget::D));
    cpu.step();
    assert_eq!(cpu.registers.d, 0xFE);
    assert_eq!(cpu.registers.f, Flags::SUBTRACT);
    cpu.registers.e = 0x01;
    write_to_pci(&mut cpu, Instruction::DEC(IncDecTarget::E));
    cpu.step();
    assert_eq!(cpu.registers.e, 0x00);
    assert_eq!(cpu.registers.f, Flags::SUBTRACT | Flags::ZERO);
}

#[test]
fn add_sp() {
    let mut cpu = CPU::init();
    write_to_pci(&mut cpu, Instruction::AddSp);
    write_to_pci_offset(&mut cpu, 1, -0x05i8 as u8);
    cpu.step();
    assert_eq!(cpu.sp, 0xFFF9);
}

#[test]
fn add() {
    let mut cpu = CPU::init();
    cpu.registers.c = 0x01;
    assert_eq!(cpu.registers.a, 0x01);
    write_to_pci(&mut cpu, Instruction::ADD(ArithmeticTarget::C));
    cpu.step();
    assert_eq!(cpu.registers.get_a(), 0x02);
    cpu.registers.d = 0x02;
    write_to_pci(&mut cpu, Instruction::ADD(ArithmeticTarget::D));
    cpu.step();
    assert_eq!(cpu.registers.get_a(), 0x04);
}

#[test]
fn daa() {
    let mut cpu = CPU::init();
    cpu.registers.a = 0x26;
    cpu.registers.b = 0x26;
    write_to_pci(&mut cpu, Instruction::ADD(ArithmeticTarget::B));
    cpu.step();
    assert_eq!(cpu.registers.a, 0x4C);
    write_to_pci(&mut cpu, Instruction::DAA);
    cpu.step();
    // Note that 0x52 is 26 + 26 (i.e. BCD) rather than 0x4C
    assert_eq!(cpu.registers.a, 0x52);
}

#[test]
fn add_hl() {
    let mut cpu = CPU::init();
    // Let's set ZERO so we can look at it later.
    cpu.registers.f |= Flags::ZERO;
    cpu.registers.set_hl(0x0100);
    cpu.registers.set_bc(0x1011);
    write_to_pci(&mut cpu, Instruction::AddHl(Arithmetic16Target::BC));
    cpu.step();
    assert_eq!(cpu.registers.get_hl(), 0x1111);
    assert_eq!(cpu.registers.f.zero(), true);
    cpu.registers.set_de(0x0F00);
    write_to_pci(&mut cpu, Instruction::AddHl(Arithmetic16Target::DE));
    cpu.step();
    assert_eq!(cpu.registers.get_hl(), 0x2011);
    assert_eq!(cpu.flags(), Flags::ZERO | Flags::HALF_CARRY);
}

#[test]
fn subtract() {
    let mut cpu = CPU::init();
    cpu.registers.a = 0x10;
    cpu.registers.b = 0x08;
    write_to_pci(&mut cpu, Instruction::SUB(ArithmeticTarget::B));
    cpu.step();
    assert_eq!(cpu.registers.a, 0x08);
    write_to_pci(&mut cpu, Instruction::SUB(ArithmeticTarget::D8));
    write_to_pci_offset(&mut cpu, 1, 0x04);
    cpu.step();
    assert_eq!(cpu.registers.a, 0x04);
}

#[test]
fn subtract_with_carry() {
    let mut cpu = CPU::init();
    cpu.registers.f.set_carry(true);
    cpu.registers.a = 0x15;
    cpu.registers.b = 0x5;
    write_to_pci(&mut cpu, Instruction::SBC(ArithmeticTarget::B));
    cpu.step();
    assert_eq!(cpu.registers.a, 0x0F);
    assert!(cpu.registers.f.subtract());
}

#[test]
fn carry_works_too() {
    let mut cpu = CPU::new();
    cpu.registers.a = 0x3A;
    cpu.registers.b = 0xC6;
    cpu.execute(OpCode::from_instruction(Instruction::ADD(
        ArithmeticTarget::B,
    )));
    assert_eq!(cpu.registers.get_a(), 0);
    assert!(cpu.registers.f.zero());
    assert!(cpu.registers.f.carry());
    assert!(cpu.registers.f.half_carry());
}

#[test]
fn add_immediate_carry_works() {
    let mut cpu = CPU::init();
    cpu.registers.a = 0x3C;
    assert_eq!(cpu.pc, 0x100);
    write_to_pci(&mut cpu, ADD(ArithmeticTarget::D8));
    write_to_pci_offset(&mut cpu, 1, 0xFF);
    cpu.step();
    assert_eq!(cpu.pc, 0x102);
    assert_eq!(cpu.registers.get_a(), 0x3B);
    assert!(!cpu.registers.f.zero());
    assert!(cpu.registers.f.half_carry());
    assert!(!cpu.registers.f.subtract());
    assert!(cpu.registers.f.carry());
}

#[test]
fn add_with_carry() {
    use super::flags::Flags;
    let mut cpu = CPU::init();
    cpu.registers.a = 0x10;
    cpu.registers.b = 0x10;
    cpu.registers.set_flag(Flags::CARRY, true);
    // ADC A, B
    write_to_pci(&mut cpu, ADC(ArithmeticTarget::B));
    cpu.step();
    assert_eq!(cpu.registers.a, 0x21);
    assert_eq!(cpu.registers.f.carry(), false);
    assert_eq!(cpu.registers.f.half_carry(), false);
    cpu.registers.c = 0x0F;
    // ADC A, C
    write_to_pci(&mut cpu, ADC(ArithmeticTarget::C));
    cpu.step();
    assert_eq!(cpu.registers.a, 0x30);
    assert_eq!(cpu.registers.f.carry(), false);
    assert_eq!(cpu.registers.f.half_carry(), true);
}

#[test]
fn and() {
    let mut cpu = CPU::init();
    cpu.registers.b = 0x10;
    cpu.registers.a = 0x11;
    write_to_pci(&mut cpu, Instruction::AND(ArithmeticTarget::B));
    cpu.step();
    assert_eq!(cpu.registers.a, 0x10);
    assert!(cpu.registers.f.half_carry());
    assert!(!cpu.registers.f.carry());
    assert!(!cpu.registers.f.zero());

    cpu.registers.c = 0x00;
    write_to_pci(&mut cpu, Instruction::AND(ArithmeticTarget::C));
    cpu.step();
    assert_eq!(cpu.registers.a, 0x00);
    assert_eq!(cpu.registers.f, Flags::ZERO | Flags::HALF_CARRY);
}

#[test]
fn xor_operation() {
    let mut cpu = CPU::init();
    cpu.registers.a = 0b01010101;
    cpu.registers.c = 0xFF;
    write_to_pci(&mut cpu, Instruction::XOR(ArithmeticTarget::C));
    cpu.step();
    assert_eq!(cpu.registers.a, 0b10101010);
    assert_eq!(cpu.registers.f, Flags::empty());
}

#[test]
fn or_operation() {
    let mut cpu = CPU::init();
    cpu.registers.a = 0x00;
    cpu.registers.b = 0x0F;
    write_to_pci(&mut cpu, Instruction::OR(ArithmeticTarget::B));
    cpu.step();
    assert_eq!(cpu.registers.a, 0x0F);
    assert_eq!(cpu.registers.f, Flags::empty());
    write_to_pci(&mut cpu, Instruction::OR(ArithmeticTarget::D8));
    write_to_pci_offset(&mut cpu, 1, 0xF0);
    cpu.step();
    assert_eq!(cpu.registers.a, 0xFF);
}

#[test]
fn cp_operation() {
    let mut cpu = CPU::init();
    cpu.registers.a = 0x10;
    cpu.registers.b = 0x08;
    write_to_pci(&mut cpu, Instruction::CP(ArithmeticTarget::B));
    cpu.step();
    assert_eq!(cpu.registers.a, 0x10);
    assert_eq!(cpu.registers.f, Flags::SUBTRACT | Flags::HALF_CARRY);
}

#[test]
fn ccf() {
    let mut cpu = CPU::init();
    assert_eq!(
        cpu.registers.f,
        Flags::ZERO | Flags::HALF_CARRY | Flags::CARRY
    );
    write_to_pci(&mut cpu, Instruction::CCF);
    cpu.step();
    assert_eq!(cpu.registers.f, Flags::ZERO);
}

#[test]
fn scf() {
    let mut cpu = CPU::init();
    cpu.registers.f.set_carry(false);
    write_to_pci(&mut cpu, Instruction::SCF);
    cpu.step();
    assert_eq!(cpu.registers.f, Flags::ZERO | Flags::CARRY);
}

#[test]
fn rlca() {
    let mut cpu = CPU::init();
    cpu.registers.f.set_carry(false);
    cpu.registers.a = 0b10000001;
    write_to_pci(&mut cpu, Instruction::RLCA);
    cpu.step();
    assert!(cpu.registers.f.contains(Flags::CARRY));
    assert_eq!(cpu.registers.a, 0b00000011);
    write_to_pci(&mut cpu, Instruction::RLCA);
    cpu.step();
    assert_eq!(cpu.registers.f, Flags::empty());
    assert_eq!(cpu.registers.a, 0b00000110);
}

#[test]
fn rla() {
    let mut cpu = CPU::init();
    cpu.registers.f.set_carry(false);
    cpu.registers.a = 0b10000001;
    write_to_pci(&mut cpu, Instruction::RLA);
    cpu.step();
    assert!(cpu.registers.f.contains(Flags::CARRY));
    assert_eq!(cpu.registers.a, 0b00000010);
    write_to_pci(&mut cpu, Instruction::RLA);
    cpu.step();
    assert_eq!(cpu.registers.f, Flags::empty());
    assert_eq!(cpu.registers.a, 0b00000101);
}

#[test]
fn rrca() {
    let mut cpu = CPU::init();
    cpu.registers.f.set_carry(false);
    cpu.registers.a = 0b10000001;
    write_to_pci(&mut cpu, Instruction::RRCA);
    cpu.step();
    assert!(cpu.registers.f.contains(Flags::CARRY));
    assert_eq!(cpu.registers.a, 0b11000000);
    write_to_pci(&mut cpu, Instruction::RRCA);
    cpu.step();
    assert_eq!(cpu.registers.f, Flags::empty());
    assert_eq!(cpu.registers.a, 0b01100000);
}

#[test]
fn rra() {
    let mut cpu = CPU::init();
    cpu.registers.f.set_carry(false);
    cpu.registers.a = 0b10000001;
    write_to_pci(&mut cpu, Instruction::RRA);
    cpu.step();
    assert_eq!(cpu.registers.a, 0b01000000);
    assert!(cpu.registers.f.contains(Flags::CARRY));
    write_to_pci(&mut cpu, Instruction::RRA);
    cpu.step();
    assert_eq!(cpu.registers.f, Flags::empty());
    assert_eq!(cpu.registers.a, 0b10100000);
}

#[test]
fn complement() {
    let mut cpu = CPU::init();
    cpu.registers.a = 0xF0;
    write_to_pci(&mut cpu, Instruction::CPL);
    cpu.step();
    assert_eq!(cpu.registers.a, 0x0F);
    assert_eq!(cpu.flags().half_carry(), true);
    assert_eq!(cpu.flags().subtract(), true);
}

#[test]
fn bit_check() {
    let mut cpu = CPU::init();
    cpu.registers.b = 0b01000000;
    write_prefix_instruction(
        &mut cpu,
        Instruction::BIT(BitPosition::SIX, LoadByteTarget::B),
    );
    cpu.step();
    assert!(!cpu.flags().zero());
}

#[test]
fn reset() {
    let mut cpu = CPU::init();
    cpu.registers.b = 0b01000000;
    write_prefix_instruction(
        &mut cpu,
        Instruction::RESET(BitPosition::SIX, LoadByteTarget::B),
    );
    cpu.step();
    assert_eq!(cpu.registers.b, 0);
    cpu.bus.write_byte(0x1000, 0b11000000);
    cpu.registers.set_hl(0x1000);
    write_prefix_instruction(
        &mut cpu,
        Instruction::RESET(BitPosition::SEVEN, LoadByteTarget::HLI),
    );
    cpu.step();
    assert_eq!(cpu.bus.read_byte(0x1000), 0b01000000);
}

#[test]
fn set() {
    let mut cpu = CPU::init();
    cpu.registers.b = 0b01000000;
    write_prefix_instruction(
        &mut cpu,
        Instruction::SET(BitPosition::FIVE, LoadByteTarget::B),
    );
    cpu.step();
    assert_eq!(cpu.registers.b, 0b01100000);
    cpu.bus.write_byte(0x1000, 0b01000000);
    cpu.registers.set_hl(0x1000);
    write_prefix_instruction(
        &mut cpu,
        Instruction::SET(BitPosition::SEVEN, LoadByteTarget::HLI),
    );
    cpu.step();
    assert_eq!(cpu.bus.read_byte(0x1000), 0b11000000);
}

#[test]
fn shift_right_arithmetic() {
    let mut cpu = CPU::init();
    cpu.registers.b = 0b11000001;
    write_prefix_instruction(&mut cpu, Instruction::SRA(LoadByteTarget::B));
    cpu.registers.f.set_carry(false);
    cpu.step();
    assert_eq!(cpu.registers.b, 0b11100000);
    assert_eq!(cpu.flags().carry(), true);
    assert_eq!(cpu.flags().zero(), false);
    write_byte(0x1000, &mut cpu, 0x01);
    cpu.registers.set_hl(0x1000);
    write_prefix_instruction(&mut cpu, Instruction::SRA(LoadByteTarget::HLI));
    cpu.step();
    assert_eq!(cpu.bus.read_byte(0x1000), 0x00);
    assert_eq!(cpu.flags().carry(), true);
    assert_eq!(cpu.flags().zero(), true);
}

#[test]
fn shift_left_arithmetic() {
    let mut cpu = CPU::init();
    cpu.registers.f.set_carry(false);
    cpu.registers.b = 0b11000001;
    write_prefix_instruction(&mut cpu, Instruction::SLA(LoadByteTarget::B));
    cpu.step();
    assert_eq!(cpu.registers.b, 0b10000010);
    assert_eq!(cpu.flags().carry(), true);
    assert_eq!(cpu.flags().zero(), false);
    write_byte(0x1000, &mut cpu, 0b10000000);
    cpu.registers.set_hl(0x1000);
    write_prefix_instruction(&mut cpu, Instruction::SLA(LoadByteTarget::HLI));
    cpu.step();
    assert_eq!(cpu.bus.read_byte(0x1000), 0x00);
    assert_eq!(cpu.flags().carry(), true);
    assert_eq!(cpu.flags().zero(), true);
}

#[test]
fn swap() {
    let mut cpu = CPU::init();
    cpu.registers.b = 0x80;
    write_prefix_instruction(&mut cpu, Instruction::SWAP(LoadByteTarget::B));
    cpu.step();
    assert_eq!(cpu.registers.b, 0x08);
}

#[test]
fn srl() {
    let mut cpu = CPU::init();
    cpu.registers.b = 0x80;
    write_prefix_instruction(&mut cpu, Instruction::SRL(LoadByteTarget::B));
    cpu.step();
    assert_eq!(cpu.registers.b, 0x40);
}

#[test]
fn jump_instructions() {
    let mut cpu = CPU::init();
    write_to_pci(&mut cpu, Instruction::JP(JumpTest::Always));
    write_to_pci_offset(&mut cpu, 1, 0);
    write_to_pci_offset(&mut cpu, 2, 0x21);
    cpu.step();
    assert_eq!(cpu.pc, 0x2100);
}

#[test]
fn jump_relative() {
    let mut cpu = CPU::init();
    assert_eq!(cpu.pc, 0x100);
    write_to_pci(&mut cpu, Instruction::JR(JumpTest::Always));
    write_to_pci_offset(&mut cpu, 1, -1i8 as u8);
    cpu.step();
    assert_eq!(cpu.pc, 0xFF);
    write_to_pci(&mut cpu, Instruction::JR(JumpTest::Always));
    write_to_pci_offset(&mut cpu, 1, 4i8 as u8);
    cpu.step();
    assert_eq!(cpu.pc, 0x103);
}

#[test]
fn rst() {
    let mut cpu = CPU::init();
    // Just jumping to a different spot to make it easier
    // to show PUSH order
    cpu.pc = 0x0200;
    assert_eq!(cpu.pc, 0x0200);
    assert_eq!(cpu.sp, 0xFFFE);
    assert_eq!(cpu.bus.read_byte(cpu.sp), 0x00);
    write_to_pci(&mut cpu, Instruction::RST(RstTarget::X00));
    cpu.step();
    assert_eq!(cpu.pc, 0x0000);
    // The previous next PC is pushed to the stack
    assert_eq!(cpu.bus.read_byte(0xFFFD), 0x02);
    // Then the Hi bit of the new PC is pushed
    assert_eq!(cpu.bus.read_byte(0xFFFC), 0x01);
    // The stack pointer has been decremented twice.
    assert_eq!(cpu.sp, 0xFFFC);
}

#[test]
fn load_weird_sp_hl_thing() {
    let mut cpu = CPU::init();
    assert_eq!(cpu.sp, 0xFFFE);
    write_to_pci(&mut cpu, Instruction::LD(LoadType::HlSpE));
    write_to_pci_offset(&mut cpu, 1, -0x02i8 as u8);
    cpu.step();
    assert_eq!(cpu.registers.get_hl(), 0xFFFC);
    assert_eq!(cpu.flags(), Flags::CARRY);
    cpu.sp = 0xFF11;
    write_to_pci(&mut cpu, Instruction::LD(LoadType::HlSpE));
    write_to_pci_offset(&mut cpu, 1, 0xFF);
    cpu.step();
    assert_eq!(cpu.registers.get_hl(), 0xFF10);
    assert_eq!(cpu.flags(), Flags::CARRY);
    write_to_pci(&mut cpu, Instruction::LD(LoadType::SpHl));
    cpu.step();
    assert_eq!(cpu.sp, 0xFF10);
}

#[test]
fn load_byte_instructions() {
    use LoadType::*;
    let mut cpu = CPU::init();
    assert_eq!(cpu.pc, 0x100);
    write_to_pci(&mut cpu, LD(Byte(LoadByteTarget::B, LoadByteSource::C)));
    cpu.registers.c = 0x99;
    cpu.step();
    assert_eq!(cpu.registers.b, 0x99);
    assert_eq!(cpu.pc, 0x101);

    write_to_pci(&mut cpu, LD(Byte(LoadByteTarget::C, LoadByteSource::D8)));
    write_to_pci_offset(&mut cpu, 1, 0xAA);
    cpu.step();
    assert_eq!(cpu.registers.c, 0xAA);
    // Assert that PC incremented by two becuase
    // of the wide instruction.
    assert_eq!(cpu.pc, 0x103);

    // Check that loading from HLI to registers works
    cpu.registers.set_hl(0x9999);
    write_byte(0x9999, &mut cpu, 0xAB);
    // LD D <- (HL)
    write_to_pci(&mut cpu, LD(Byte(LoadByteTarget::D, LoadByteSource::HLI)));
    cpu.step();
    // This will have to get the address (0x9999) from the HL registers,
    // then look at memory address 0x9999 for the value, then write that value
    // register d.
    assert_eq!(cpu.registers.d, 0xAB);
    assert_eq!(cpu.pc, 0x104);

    // Check loading _to_ HLI from wherever.
    cpu.registers.c = 0x12;
    // Just to prove that it's empty before this whole deal.
    assert_eq!(cpu.bus.read_byte(0x1234), 0x0);
    cpu.registers.set_hl(0x1234);
    write_to_pci(&mut cpu, LD(Byte(LoadByteTarget::HLI, LoadByteSource::C)));
    cpu.step();
    assert_eq!(cpu.bus.read_byte(0x1234), 0x12);
}

#[test]
fn load_indirect_byte() {
    use LoadType::{ IndirectByteFromA, IndirectByteToA };
    let mut cpu = CPU::init();
    cpu.registers.a = 0x01;
    cpu.registers.set_bc(0x0500);
    write_to_pci(
        &mut cpu,
        Instruction::LD(IndirectByteFromA(LoadIndirectByteTarget::BC)),
    );
    cpu.step();
    assert_eq!(cpu.bus.read_byte(0x0500), 0x01);
    cpu.bus.write_byte(0x0750, 0x50);
    cpu.registers.set_de(0x750);
    write_to_pci(&mut cpu, Instruction::LD(IndirectByteToA(LoadIndirectByteTarget::DE)));
    cpu.step();
    assert_eq!(cpu.registers.a, 0x50);

    cpu.bus.write_byte(0x0800, 0x99);
    cpu.registers.set_hl(0x0800);
    write_to_pci(&mut cpu, Instruction::LD(IndirectByteToA(LoadIndirectByteTarget::HlDec)));
    cpu.step();

    cpu.registers.a = 0x99;
    cpu.bus.write_word(cpu.pc + 1, 0x13FF);
    write_to_pci(&mut cpu, Instruction::LD(LoadType::IndirectByteFromA(LoadIndirectByteTarget::A16)));
    cpu.step();
    assert_eq!(cpu.bus.read_byte(0x13FF), 0x99);
}

#[test]
fn load_high_byte_instructions() {
    let mut cpu = CPU::init();
    cpu.registers.a = 0xFF;
    assert_eq!(cpu.bus.read_byte(0xFF55), 0x00);
    write_to_pci(&mut cpu, Instruction::LD(LoadType::HighFromA(LoadHighByteTarget::D8)));
    write_to_pci_offset(&mut cpu, 1, 0x55);
    cpu.step();
    assert_eq!(cpu.bus.read_byte(0xFF55), 0xFF);
    cpu.registers.c = 0x11;
    cpu.registers.a = 0x99;
    write_to_pci(&mut cpu, Instruction::LD(LoadType::HighFromA(LoadHighByteTarget::C)));
    cpu.step();
    assert_eq!(cpu.bus.read_byte(0xFF11), 0x99);

    cpu.bus.write_byte(0xFF33, 0x12);
    write_to_pci(&mut cpu, Instruction::LD(LoadType::HighToA(LoadHighByteTarget::D8)));
    write_to_pci_offset(&mut cpu, 1, 0x33);
    cpu.step();
    assert_eq!(cpu.registers.a, 0x12);
}

#[test]
fn load_word_instructions() {
    use LoadType::Word;
    let mut cpu = CPU::init();
    // Prove init conditions.
    assert_eq!(cpu.pc, 0x100);
    // LD BC <- D16
    write_to_pci(&mut cpu, LD(Word(LoadWordTarget::BC, LoadWordSource::D16)));
    write_to_pci_offset(&mut cpu, 1, 0xEF);
    write_to_pci_offset(&mut cpu, 2, 0xBE);
    cpu.step();
    assert_eq!(cpu.registers.get_bc(), 0xBEEF);
    assert_eq!(cpu.pc, 0x103);

    // Throw out the old CPU for convenience.
    let mut cpu = CPU::init();
    assert_eq!(cpu.sp, 0xFFFE);
    write_to_pci(&mut cpu, LD(Word(LoadWordTarget::I16, LoadWordSource::SP)));
    cpu.bus.write_word(cpu.pc + 1, 0x1234);
    cpu.step();
    assert_eq!(cpu.bus.read_word(0x1234), 0xFFFE);
}

#[test]
fn push() {
    use StackTarget::*;
    let mut cpu = CPU::init();
    // PUSH BC
    cpu.registers.set_bc(0xBEEF);
    write_to_pci(&mut cpu, PUSH(BC));
    cpu.step();
    assert_eq!(cpu.sp, 0xFFFC);
    assert_eq!(cpu.bus.read_byte(cpu.sp + 1), 0xBE);
    assert_eq!(cpu.bus.read_byte(cpu.sp), 0xEF);
    // PUSH DE
    cpu.registers.set_de(0xEFDE);
    write_to_pci(&mut cpu, PUSH(DE));
    cpu.step();
    assert_eq!(cpu.sp, 0xFFFA);
    assert_eq!(cpu.bus.read_byte(cpu.sp + 1), 0xEF);
    assert_eq!(cpu.bus.read_byte(cpu.sp), 0xDE);
    // PUSH HL
    cpu.registers.set_hl(0xDEAD);
    write_to_pci(&mut cpu, PUSH(HL));
    cpu.step();
    assert_eq!(cpu.sp, 0xFFF8);
    assert_eq!(cpu.bus.read_byte(cpu.sp + 1), 0xDE);
    assert_eq!(cpu.bus.read_byte(cpu.sp), 0xAD);
    // PUSH AF
    cpu.registers.a = 0xAB;
    write_to_pci(&mut cpu, PUSH(AF));
    cpu.step();
    assert_eq!(cpu.sp, 0xFFF6);
    assert_eq!(cpu.bus.read_byte(cpu.sp + 1), 0xAB);
    // This is the initial value of the flags register
    assert_eq!(cpu.bus.read_byte(cpu.sp), 0xB0);
}

#[test]
fn pop() {
    use StackTarget::*;
    let mut cpu = CPU::init();
    cpu.registers.set_bc(0xBEEF);
    cpu.registers.set_de(0xDEAD);
    cpu.registers.set_hl(0x1234);
    // PUSH from BC
    write_to_pci(&mut cpu, PUSH(BC));
    // PUSH from DE
    write_instruction_to_pci_offset(&mut cpu, 1, PUSH(DE));
    // PUSH from HL
    write_instruction_to_pci_offset(&mut cpu, 2, PUSH(HL));
    // POP to AF
    write_instruction_to_pci_offset(&mut cpu, 3, POP(AF));
    // POP to BC
    write_instruction_to_pci_offset(&mut cpu, 4, POP(BC));
    // POP to DE
    write_instruction_to_pci_offset(&mut cpu, 5, POP(DE));
    cpu.step();
    cpu.step();
    cpu.step();
    cpu.step();
    cpu.step();
    cpu.step();
    assert_eq!(cpu.pc, 0x106);
    assert_eq!(cpu.sp, 0xFFFE);
    assert_eq!(cpu.registers.get_bc(), 0xDEAD);
    assert_eq!(cpu.registers.get_de(), 0xBEEF);
    // Last four bits of F are always 0
    assert_eq!(cpu.registers.get_af(), 0x1230);
}

#[test]
fn step_with_add() {
    let mut cpu = CPU::init();
    // This value comes from the initial contents of A
    assert_eq!(cpu.registers.get_a(), 1);
    cpu.registers.b = 10u8;

    write_to_pci(&mut cpu, ADD(ArithmeticTarget::B));
    cpu.step();
    assert_eq!(cpu.registers.get_a(), 11);
}

#[test]
fn call() {
    let mut cpu = CPU::init();
    assert_eq!(cpu.pc, 0x0100);
    // CALL
    write_to_pci(&mut cpu, CALL(JumpTest::Always));
    write_to_pci_offset(&mut cpu, 1, 0x00);
    write_to_pci_offset(&mut cpu, 2, 0x22);
    // Put a return instruction at the new PC target.
    cpu.bus.write_byte(0x2200, 0xC9);
    cpu.step();
    assert_eq!(cpu.pc, 0x2200);
    cpu.step();
    assert_eq!(cpu.pc, 0x0103);
}

#[test]
fn reti() {
    let mut cpu = CPU::init();
    // Throw this low to show it going high.
    cpu.interrupt_enabled = false;
    cpu.pc = 0x0000;
    cpu.push(0x0101);
    write_to_pci(&mut cpu, Instruction::RETI);
    cpu.step();
    assert_eq!(cpu.pc, 0x0101);
    assert_eq!(cpu.sp, 0xFFFE);
    assert_eq!(cpu.interrupt_enabled, true);
}

#[test]
fn add_hli() {
    let mut cpu = CPU::new();
    cpu.registers.a = 10u8;
    cpu.registers.set_hl(0x5000);
    cpu.bus.write_byte(0x5000, 10);
    write_to_pci(&mut cpu, ADD(ArithmeticTarget::HLI));
    cpu.step();
    assert_eq!(cpu.registers.get_a(), 20);
}
