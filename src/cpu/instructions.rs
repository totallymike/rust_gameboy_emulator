use lazy_static::lazy_static;

#[derive(Copy, Clone, Debug)]
pub struct OpCode {
    pub instruction: Instruction,
    pub number: u8,
    pub size: u8,
    // TODO: add an extra_cycles field to account for
    // TODO: instructions that have variable cycle counts, like JP(JumpTarget::ZERO) et al
    pub cycles: u8,
}

#[derive(Copy, Clone, Debug, PartialEq)]
/// CPU instructions
pub enum Instruction {
    /// Add a value to the A register, leaving the result in A.
    ADD(ArithmeticTarget),
    AddHl(Arithmetic16Target),
    /// Not so much about adding as it is about moving the stack pointer
    AddSp,
    ADC(ArithmeticTarget),
    AND(ArithmeticTarget),
    BIT(BitPosition, LoadByteTarget),
    CALL(JumpTest),
    /// Toggle the CARRY flag
    CCF,
    CP(ArithmeticTarget),
    /// Invert all the bits of A register
    CPL,
    /// Correct BCD arithmetic. Uh...see the tests.
    DAA,
    DEC(IncDecTarget),
    DEC16(IncDec16Target),
    /// System clock and CPU halt after this instruction.
    /// Some other stuff can bring it back up.
    /// Disable Interrucpt.
    /// NOTE: If this is executed in an interrupt, it'll be clobbered if RETI
    /// is used to return from the interrupt.
    DI,
    /// Enable interrupt
    EI,
    HALT,
    INC(IncDecTarget),
    INC16(IncDec16Target),
    JP(JumpTest),
    /// Jump to address contained in HL. Not conditional
    JpHl,
    JR(JumpTest),
    LD(LoadType),
    NOP,
    OR(ArithmeticTarget),
    PUSH(StackTarget),
    POP(StackTarget),
    /// A placeholder for the PREFIX value
    PREFIX,
    /// Set the target bit to 0
    RESET(BitPosition, LoadByteTarget),
    RL(LoadByteTarget),
    RLA,
    RLC(LoadByteTarget),
    RLCA,
    RR(LoadByteTarget),
    RRA,
    RRC(LoadByteTarget),
    RRCA,
    RET(JumpTest),
    /// Like RET, but always sets the master interrupt enable flag.
    RETI,
    /// I think this is for moving back and forth
    /// between the program and interrupt handlers.
    /// I'll need to see it used more to figure it out properly.
    RST(RstTarget),
    SBC(ArithmeticTarget),
    SCF,
    /// Set the target bit to 1
    SET(BitPosition, LoadByteTarget),
    /// Shift Left (Arithmetic)
    /// Bits are shifted to the left. Bit 7 is copied into the CARRY flag,
    /// bit zero is reset.
    SLA(LoadByteTarget),
    /// Shift Right (Arithmetic):
    /// All bits are shifted to the right; bit 7 is preserved.
    /// Bit 0 is shifted into the CARRY flag.
    SRA(LoadByteTarget),
    // Shift Right (Logical)
    /// All bits are shifted to the right; bit 7 is reset.
    /// Bit 0 is shifted into the CARRY flag.
    SRL(LoadByteTarget),
    /// STOP stops just about everything.
    /// To restart a real DMG either throw the /RESET terminal to LOW,
    /// or do the same to terminals P10, P11, P12, or P13.
    /// TODO: Figure out what this actually is used for.
    /// TODO: And what terminals P10-13 are connected to.
    STOP,
    SUB(ArithmeticTarget),
    SWAP(LoadByteTarget),
    /// This is for those opcodes that don't have an instruction
    UNSUPPORTED,
    XOR(ArithmeticTarget),
}

macro_rules! add_instruction {
    ($opcodes:ident, $opcode:expr) => {
        let opcode: OpCode = $opcode;
        $opcodes[opcode.number as usize] = Some(opcode);
    };
    ($opcodes:ident, $instruction:expr, $size: expr, $cycles:expr, $num: expr) => {
        match $opcodes[$num as usize] {
            None => {
                $opcodes[$num as usize] = Some(OpCode {
                    instruction: $instruction,
                    number: $num,
                    size: $size,
                    cycles: $cycles,
                });
            }
            Some(code) => panic!(
                "Already added this opcode!: {:X}, existing: {:?}",
                $num as usize, code
            ),
        };
    };
}

lazy_static! {
    pub static ref NON_PREFIXED_OPCODES: [Option<OpCode>; 256] = {
        use self::Instruction::*;
        let mut opcodes: [Option<OpCode>; 256] = [None; 256];
        add_instruction!(opcodes, NOP, 1, 4, 0x00);
        add_instruction!(
            opcodes,
            LD(LoadType::Word(LoadWordTarget::BC, LoadWordSource::D16)),
            3,
            12,
            0x01
        );
        add_instruction!(
            opcodes,
            LD(LoadType::IndirectByteFromA(LoadIndirectByteTarget::BC)),
            1,
            8,
            0x02
        );
        add_instruction!(opcodes, INC16(IncDec16Target::BC), 1, 8, 0x03);
        add_instruction!(opcodes, INC(IncDecTarget::B), 1, 4, 0x04);
        add_instruction!(opcodes, DEC(IncDecTarget::B), 1, 4, 0x05);
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::B, LoadByteSource::D8)),
            2,
            8,
            0x06
        );
        add_instruction!(opcodes, RLCA, 1, 4, 0x07);
        add_instruction!(
            opcodes,
            LD(LoadType::Word(LoadWordTarget::I16, LoadWordSource::SP)),
            3,
            20,
            0x08
        );
        add_instruction!(opcodes, AddHl(Arithmetic16Target::BC), 1, 8, 0x09);
        add_instruction!(
            opcodes,
            LD(LoadType::IndirectByteToA(LoadIndirectByteTarget::BC)),
            1,
            8,
            0x0A
        );
        add_instruction!(opcodes, DEC16(IncDec16Target::BC), 1, 8, 0x0B);
        add_instruction!(opcodes, INC(IncDecTarget::C), 1, 4, 0x0C);
        add_instruction!(opcodes, DEC(IncDecTarget::C), 1, 4, 0x0D);
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::C, LoadByteSource::D8)),
            2,
            8,
            0x0E
        );
        add_instruction!(opcodes, RRCA, 1, 4, 0x0F);
        add_instruction!(opcodes, STOP, 2, 4, 0x10);
        add_instruction!(
            opcodes,
            LD(LoadType::Word(LoadWordTarget::DE, LoadWordSource::D16)),
            3,
            12,
            0x11
        );
        add_instruction!(
            opcodes,
            LD(LoadType::IndirectByteFromA(LoadIndirectByteTarget::DE)),
            1,
            8,
            0x12
        );
        add_instruction!(opcodes, INC16(IncDec16Target::DE), 1, 8, 0x13);
        add_instruction!(opcodes, INC(IncDecTarget::D), 1, 4, 0x14);
        add_instruction!(opcodes, DEC(IncDecTarget::D), 1, 4, 0x15);
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::D, LoadByteSource::D8)),
            2,
            8,
            0x16
        );
        add_instruction!(opcodes, RLA, 1, 4, 0x17);
        add_instruction!(opcodes, JR(JumpTest::Always), 2, 12, 0x18);
        add_instruction!(opcodes, AddHl(Arithmetic16Target::DE), 1, 8, 0x19);
        add_instruction!(
            opcodes,
            LD(LoadType::IndirectByteToA(LoadIndirectByteTarget::DE)),
            1,
            8,
            0x1A
        );
        add_instruction!(opcodes, DEC16(IncDec16Target::DE), 1, 8, 0x1B);
        add_instruction!(opcodes, INC(IncDecTarget::E), 1, 4, 0x1C);
        add_instruction!(opcodes, DEC(IncDecTarget::E), 1, 4, 0x1D);
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::E, LoadByteSource::D8)),
            2,
            8,
            0x1E
        );
        add_instruction!(opcodes, RRA, 1, 4, 0x1F);
        add_instruction!(opcodes, JR(JumpTest::NotZero), 2, 12, 0x20);
        add_instruction!(
            opcodes,
            LD(LoadType::Word(LoadWordTarget::HL, LoadWordSource::D16)),
            3,
            12,
            0x21
        );
        add_instruction!(
            opcodes,
            LD(LoadType::IndirectByteFromA(LoadIndirectByteTarget::HlInc)),
            1,
            8,
            0x22
        );
        add_instruction!(opcodes, INC16(IncDec16Target::HL), 1, 8, 0x23);
        add_instruction!(opcodes, INC(IncDecTarget::H), 1, 4, 0x24);
        add_instruction!(opcodes, DEC(IncDecTarget::H), 1, 4, 0x25);
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::H, LoadByteSource::D8)),
            2,
            8,
            0x26
        );
        add_instruction!(opcodes, DAA, 1, 4, 0x27);
        add_instruction!(opcodes, JR(JumpTest::Zero), 2, 12, 0x28);
        add_instruction!(opcodes, AddHl(Arithmetic16Target::HL), 1, 8, 0x29);
        add_instruction!(
            opcodes,
            LD(LoadType::IndirectByteToA(LoadIndirectByteTarget::HlInc)),
            1,
            8,
            0x2A
        );
        add_instruction!(opcodes, DEC16(IncDec16Target::HL), 1, 8, 0x2B);
        add_instruction!(opcodes, INC(IncDecTarget::L), 1, 4, 0x2C);
        add_instruction!(opcodes, DEC(IncDecTarget::L), 1, 4, 0x2D);
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::L, LoadByteSource::D8)),
            2,
            8,
            0x2E
        );
        add_instruction!(opcodes, CPL, 1, 4, 0x2F);
        add_instruction!(opcodes, JR(JumpTest::NotCarry), 2, 12, 0x30);
        add_instruction!(
            opcodes,
            LD(LoadType::Word(LoadWordTarget::SP, LoadWordSource::D16)),
            3,
            12,
            0x31
        );
        add_instruction!(
            opcodes,
            LD(LoadType::IndirectByteFromA(LoadIndirectByteTarget::HlDec)),
            1,
            8,
            0x32
        );
        add_instruction!(opcodes, INC16(IncDec16Target::SP), 1, 8, 0x33);
        add_instruction!(opcodes, INC(IncDecTarget::HLI), 1, 12, 0x34);
        add_instruction!(opcodes, DEC(IncDecTarget::HLI), 1, 12, 0x35);
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::HLI, LoadByteSource::D8)),
            2,
            12,
            0x36
        );
        add_instruction!(opcodes, SCF, 1, 4, 0x37);
        add_instruction!(opcodes, JR(JumpTest::Carry), 2, 12, 0x38);
        add_instruction!(opcodes, AddHl(Arithmetic16Target::SP), 1, 8, 0x39);
        add_instruction!(
            opcodes,
            LD(LoadType::IndirectByteToA(LoadIndirectByteTarget::HlDec)),
            1,
            8,
            0x3A
        );
        add_instruction!(opcodes, DEC16(IncDec16Target::SP), 1, 8, 0x3B);
        add_instruction!(opcodes, INC(IncDecTarget::A), 1, 4, 0x3C);
        add_instruction!(opcodes, DEC(IncDecTarget::A), 1, 4, 0x3D);
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::A, LoadByteSource::D8)),
            2,
            8,
            0x3E
        );
        add_instruction!(opcodes, CCF, 1, 4, 0x3F);
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::B, LoadByteSource::B)),
            1,
            4,
            0x40
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::B, LoadByteSource::C)),
            1,
            4,
            0x41
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::B, LoadByteSource::D)),
            1,
            4,
            0x42
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::B, LoadByteSource::E)),
            1,
            4,
            0x43
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::B, LoadByteSource::H)),
            1,
            4,
            0x44
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::B, LoadByteSource::L)),
            1,
            4,
            0x45
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::B, LoadByteSource::HLI)),
            1,
            4,
            0x46
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::B, LoadByteSource::A)),
            1,
            4,
            0x47
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::C, LoadByteSource::B)),
            1,
            4,
            0x48
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::C, LoadByteSource::C)),
            1,
            4,
            0x49
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::C, LoadByteSource::D)),
            1,
            4,
            0x4A
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::C, LoadByteSource::E)),
            1,
            4,
            0x4B
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::C, LoadByteSource::H)),
            1,
            4,
            0x4C
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::C, LoadByteSource::L)),
            1,
            4,
            0x4D
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::C, LoadByteSource::HLI)),
            1,
            4,
            0x4E
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::C, LoadByteSource::A)),
            1,
            4,
            0x4F
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::D, LoadByteSource::B)),
            1,
            4,
            0x50
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::D, LoadByteSource::C)),
            1,
            4,
            0x51
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::D, LoadByteSource::D)),
            1,
            4,
            0x52
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::D, LoadByteSource::E)),
            1,
            4,
            0x53
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::D, LoadByteSource::H)),
            1,
            4,
            0x54
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::D, LoadByteSource::L)),
            1,
            4,
            0x55
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::D, LoadByteSource::HLI)),
            1,
            4,
            0x56
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::D, LoadByteSource::A)),
            1,
            4,
            0x57
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::E, LoadByteSource::B)),
            1,
            4,
            0x58
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::E, LoadByteSource::C)),
            1,
            4,
            0x59
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::E, LoadByteSource::D)),
            1,
            4,
            0x5A
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::E, LoadByteSource::E)),
            1,
            4,
            0x5B
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::E, LoadByteSource::H)),
            1,
            4,
            0x5C
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::E, LoadByteSource::L)),
            1,
            4,
            0x5D
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::E, LoadByteSource::HLI)),
            1,
            4,
            0x5E
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::E, LoadByteSource::A)),
            1,
            4,
            0x5F
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::H, LoadByteSource::B)),
            1,
            4,
            0x60
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::H, LoadByteSource::C)),
            1,
            4,
            0x61
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::H, LoadByteSource::D)),
            1,
            4,
            0x62
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::H, LoadByteSource::E)),
            1,
            4,
            0x63
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::H, LoadByteSource::H)),
            1,
            4,
            0x64
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::H, LoadByteSource::L)),
            1,
            4,
            0x65
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::H, LoadByteSource::HLI)),
            1,
            4,
            0x66
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::H, LoadByteSource::A)),
            1,
            4,
            0x67
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::L, LoadByteSource::B)),
            1,
            4,
            0x68
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::L, LoadByteSource::C)),
            1,
            4,
            0x69
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::L, LoadByteSource::D)),
            1,
            4,
            0x6A
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::L, LoadByteSource::E)),
            1,
            4,
            0x6B
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::L, LoadByteSource::H)),
            1,
            4,
            0x6C
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::L, LoadByteSource::L)),
            1,
            4,
            0x6D
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::L, LoadByteSource::HLI)),
            1,
            4,
            0x6E
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::L, LoadByteSource::A)),
            1,
            4,
            0x6F
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::HLI, LoadByteSource::B)),
            1,
            8,
            0x70
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::HLI, LoadByteSource::C)),
            1,
            8,
            0x71
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::HLI, LoadByteSource::D)),
            1,
            8,
            0x72
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::HLI, LoadByteSource::E)),
            1,
            8,
            0x73
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::HLI, LoadByteSource::H)),
            1,
            8,
            0x74
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::HLI, LoadByteSource::L)),
            1,
            8,
            0x75
        );
        add_instruction!(opcodes, HALT, 1, 4, 0x76);
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::HLI, LoadByteSource::A)),
            1,
            8,
            0x77
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::A, LoadByteSource::B)),
            1,
            4,
            0x78
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::A, LoadByteSource::C)),
            1,
            4,
            0x79
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::A, LoadByteSource::D)),
            1,
            4,
            0x7A
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::A, LoadByteSource::E)),
            1,
            4,
            0x7B
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::A, LoadByteSource::H)),
            1,
            4,
            0x7C
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::A, LoadByteSource::L)),
            1,
            4,
            0x7D
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::A, LoadByteSource::HLI)),
            1,
            4,
            0x7E
        );
        add_instruction!(
            opcodes,
            LD(LoadType::Byte(LoadByteTarget::A, LoadByteSource::A)),
            1,
            4,
            0x7F
        );
        add_instruction!(opcodes, ADD(ArithmeticTarget::B), 1, 4, 0x80);
        add_instruction!(opcodes, ADD(ArithmeticTarget::C), 1, 4, 0x81);
        add_instruction!(opcodes, ADD(ArithmeticTarget::D), 1, 4, 0x82);
        add_instruction!(opcodes, ADD(ArithmeticTarget::E), 1, 4, 0x83);
        add_instruction!(opcodes, ADD(ArithmeticTarget::H), 1, 4, 0x84);
        add_instruction!(opcodes, ADD(ArithmeticTarget::L), 1, 4, 0x85);
        add_instruction!(opcodes, ADD(ArithmeticTarget::HLI), 1, 8, 0x86);
        add_instruction!(opcodes, ADD(ArithmeticTarget::A), 1, 4, 0x87);
        add_instruction!(opcodes, ADC(ArithmeticTarget::B), 1, 4, 0x88);
        add_instruction!(opcodes, ADC(ArithmeticTarget::C), 1, 4, 0x89);
        add_instruction!(opcodes, ADC(ArithmeticTarget::D), 1, 4, 0x8A);
        add_instruction!(opcodes, ADC(ArithmeticTarget::E), 1, 4, 0x8B);
        add_instruction!(opcodes, ADC(ArithmeticTarget::H), 1, 4, 0x8C);
        add_instruction!(opcodes, ADC(ArithmeticTarget::L), 1, 4, 0x8D);
        add_instruction!(opcodes, ADC(ArithmeticTarget::HLI), 1, 8, 0x8E);
        add_instruction!(opcodes, ADC(ArithmeticTarget::A), 1, 4, 0x8F);
        add_instruction!(opcodes, SUB(ArithmeticTarget::B), 1, 4, 0x90);
        add_instruction!(opcodes, SUB(ArithmeticTarget::C), 1, 4, 0x91);
        add_instruction!(opcodes, SUB(ArithmeticTarget::D), 1, 4, 0x92);
        add_instruction!(opcodes, SUB(ArithmeticTarget::E), 1, 4, 0x93);
        add_instruction!(opcodes, SUB(ArithmeticTarget::H), 1, 4, 0x94);
        add_instruction!(opcodes, SUB(ArithmeticTarget::L), 1, 4, 0x95);
        add_instruction!(opcodes, SUB(ArithmeticTarget::HLI), 1, 8, 0x96);
        add_instruction!(opcodes, SUB(ArithmeticTarget::A), 1, 4, 0x97);
        add_instruction!(opcodes, SBC(ArithmeticTarget::B), 1, 4, 0x98);
        add_instruction!(opcodes, SBC(ArithmeticTarget::C), 1, 4, 0x99);
        add_instruction!(opcodes, SBC(ArithmeticTarget::D), 1, 4, 0x9A);
        add_instruction!(opcodes, SBC(ArithmeticTarget::E), 1, 4, 0x9B);
        add_instruction!(opcodes, SBC(ArithmeticTarget::H), 1, 4, 0x9C);
        add_instruction!(opcodes, SBC(ArithmeticTarget::L), 1, 4, 0x9D);
        add_instruction!(opcodes, SBC(ArithmeticTarget::HLI), 1, 8, 0x9E);
        add_instruction!(opcodes, SBC(ArithmeticTarget::A), 1, 4, 0x9F);
        add_instruction!(opcodes, AND(ArithmeticTarget::B), 1, 4, 0xA0);
        add_instruction!(opcodes, AND(ArithmeticTarget::C), 1, 4, 0xA1);
        add_instruction!(opcodes, AND(ArithmeticTarget::D), 1, 4, 0xA2);
        add_instruction!(opcodes, AND(ArithmeticTarget::E), 1, 4, 0xA3);
        add_instruction!(opcodes, AND(ArithmeticTarget::H), 1, 4, 0xA4);
        add_instruction!(opcodes, AND(ArithmeticTarget::L), 1, 4, 0xA5);
        add_instruction!(opcodes, AND(ArithmeticTarget::HLI), 1, 8, 0xA6);
        add_instruction!(opcodes, AND(ArithmeticTarget::A), 1, 4, 0xA7);
        add_instruction!(opcodes, XOR(ArithmeticTarget::B), 1, 4, 0xA8);
        add_instruction!(opcodes, XOR(ArithmeticTarget::C), 1, 4, 0xA9);
        add_instruction!(opcodes, XOR(ArithmeticTarget::D), 1, 4, 0xAA);
        add_instruction!(opcodes, XOR(ArithmeticTarget::E), 1, 4, 0xAB);
        add_instruction!(opcodes, XOR(ArithmeticTarget::H), 1, 4, 0xAC);
        add_instruction!(opcodes, XOR(ArithmeticTarget::L), 1, 4, 0xAD);
        add_instruction!(opcodes, XOR(ArithmeticTarget::HLI), 1, 8, 0xAE);
        add_instruction!(opcodes, XOR(ArithmeticTarget::A), 1, 4, 0xAF);
        add_instruction!(opcodes, OR(ArithmeticTarget::B), 1, 4, 0xB0);
        add_instruction!(opcodes, OR(ArithmeticTarget::C), 1, 4, 0xB1);
        add_instruction!(opcodes, OR(ArithmeticTarget::D), 1, 4, 0xB2);
        add_instruction!(opcodes, OR(ArithmeticTarget::E), 1, 4, 0xB3);
        add_instruction!(opcodes, OR(ArithmeticTarget::H), 1, 4, 0xB4);
        add_instruction!(opcodes, OR(ArithmeticTarget::L), 1, 4, 0xB5);
        add_instruction!(opcodes, OR(ArithmeticTarget::HLI), 1, 8, 0xB6);
        add_instruction!(opcodes, OR(ArithmeticTarget::A), 1, 4, 0xB7);
        add_instruction!(opcodes, CP(ArithmeticTarget::B), 1, 4, 0xB8);
        add_instruction!(opcodes, CP(ArithmeticTarget::C), 1, 4, 0xB9);
        add_instruction!(opcodes, CP(ArithmeticTarget::D), 1, 4, 0xBA);
        add_instruction!(opcodes, CP(ArithmeticTarget::E), 1, 4, 0xBB);
        add_instruction!(opcodes, CP(ArithmeticTarget::H), 1, 4, 0xBC);
        add_instruction!(opcodes, CP(ArithmeticTarget::L), 1, 4, 0xBD);
        add_instruction!(opcodes, CP(ArithmeticTarget::HLI), 1, 8, 0xBE);
        add_instruction!(opcodes, CP(ArithmeticTarget::A), 1, 4, 0xBF);
        add_instruction!(opcodes, RET(JumpTest::NotZero), 1, 4, 0xC0);
        add_instruction!(opcodes, POP(StackTarget::BC), 1, 12, 0xC1);
        add_instruction!(opcodes, JP(JumpTest::NotZero), 3, 16, 0xC2);
        add_instruction!(opcodes, JP(JumpTest::Always), 3, 16, 0xC3);
        add_instruction!(opcodes, CALL(JumpTest::NotZero), 1, 4, 0xC4);
        add_instruction!(opcodes, PUSH(StackTarget::BC), 1, 16, 0xC5);
        add_instruction!(opcodes, ADD(ArithmeticTarget::D8), 2, 8, 0xC6);
        add_instruction!(opcodes, RST(RstTarget::X00), 1, 16, 0xC7);
        add_instruction!(opcodes, RET(JumpTest::Zero), 1, 4, 0xC8);
        add_instruction!(opcodes, RET(JumpTest::Always), 1, 4, 0xC9);
        add_instruction!(opcodes, JP(JumpTest::Zero), 1, 16, 0xCA);
        add_instruction!(opcodes, PREFIX, 0, 0, 0xCB);
        add_instruction!(opcodes, CALL(JumpTest::Zero), 1, 4, 0xCC);
        add_instruction!(opcodes, CALL(JumpTest::Always), 1, 4, 0xCD);
        add_instruction!(opcodes, ADC(ArithmeticTarget::D8), 2, 8, 0xCE);
        add_instruction!(opcodes, RST(RstTarget::X08), 1, 16, 0xCF);
        add_instruction!(opcodes, RET(JumpTest::NotCarry), 1, 4, 0xD0);
        add_instruction!(opcodes, POP(StackTarget::DE), 1, 12, 0xD1);
        add_instruction!(opcodes, JP(JumpTest::NotCarry), 3, 16, 0xD2);
        add_instruction!(opcodes, UNSUPPORTED, 0, 1, 0xD3);
        add_instruction!(opcodes, CALL(JumpTest::NotCarry), 1, 4, 0xD4);
        add_instruction!(opcodes, PUSH(StackTarget::DE), 1, 16, 0xD5);
        add_instruction!(opcodes, SUB(ArithmeticTarget::D8), 2, 8, 0xD6);
        add_instruction!(opcodes, RST(RstTarget::X10), 1, 16, 0xD7);
        add_instruction!(opcodes, RET(JumpTest::Carry), 1, 4, 0xD8);
        add_instruction!(opcodes, RETI, 1, 16, 0xD9);
        add_instruction!(opcodes, JP(JumpTest::Carry), 1, 16, 0xDA);
        add_instruction!(opcodes, UNSUPPORTED, 0, 1, 0xDB);
        add_instruction!(opcodes, CALL(JumpTest::Carry), 1, 4, 0xDC);
        add_instruction!(opcodes, UNSUPPORTED, 0, 1, 0xDD);
        add_instruction!(opcodes, SBC(ArithmeticTarget::D8), 2, 8, 0xDE);
        add_instruction!(opcodes, RST(RstTarget::X18), 1, 16, 0xDF);
        add_instruction!(
            opcodes,
            LD(LoadType::HighFromA(LoadHighByteTarget::D8)),
            2,
            12,
            0xE0
        );
        add_instruction!(opcodes, POP(StackTarget::HL), 1, 12, 0xE1);
        add_instruction!(
            opcodes,
            LD(LoadType::HighFromA(LoadHighByteTarget::C)),
            2,
            8,
            0xE2
        );
        add_instruction!(opcodes, UNSUPPORTED, 0, 1, 0xE3);
        add_instruction!(opcodes, UNSUPPORTED, 0, 1, 0xE4);
        add_instruction!(opcodes, PUSH(StackTarget::HL), 1, 16, 0xE5);
        add_instruction!(opcodes, AND(ArithmeticTarget::D8), 2, 8, 0xE6);
        add_instruction!(opcodes, RST(RstTarget::X20), 1, 16, 0xE7);
        add_instruction!(opcodes, AddSp, 2, 16, 0xE8);
        add_instruction!(opcodes, JpHl, 1, 4, 0xE9);
        add_instruction!(
            opcodes,
            LD(LoadType::IndirectByteFromA(LoadIndirectByteTarget::A16)),
            3,
            16,
            0xEA
        );
        add_instruction!(opcodes, UNSUPPORTED, 0, 1, 0xEB);
        add_instruction!(opcodes, UNSUPPORTED, 0, 1, 0xEC);
        add_instruction!(opcodes, UNSUPPORTED, 0, 1, 0xED);
        add_instruction!(opcodes, XOR(ArithmeticTarget::D8), 2, 8, 0xEE);
        add_instruction!(opcodes, RST(RstTarget::X28), 1, 16, 0xEF);
        add_instruction!(
            opcodes,
            LD(LoadType::HighToA(LoadHighByteTarget::D8)),
            2,
            12,
            0xF0
        );
        add_instruction!(opcodes, POP(StackTarget::AF), 1, 12, 0xF1);
        add_instruction!(
            opcodes,
            LD(LoadType::HighToA(LoadHighByteTarget::C)),
            2,
            8,
            0xF2
        );
        add_instruction!(opcodes, DI, 1, 4, 0xF3);
        add_instruction!(opcodes, UNSUPPORTED, 0, 1, 0xF4);
        add_instruction!(opcodes, PUSH(StackTarget::AF), 1, 16, 0xF5);
        add_instruction!(opcodes, OR(ArithmeticTarget::D8), 2, 8, 0xF6);
        add_instruction!(opcodes, RST(RstTarget::X30), 1, 16, 0xF7);
        add_instruction!(opcodes, LD(LoadType::HlSpE), 2, 12, 0xF8);
        add_instruction!(opcodes, LD(LoadType::SpHl), 1, 8, 0xF9);
        add_instruction!(
            opcodes,
            LD(LoadType::IndirectByteToA(LoadIndirectByteTarget::A16)),
            3,
            16,
            0xFA
        );
        add_instruction!(opcodes, EI, 1, 4, 0xFB);
        add_instruction!(opcodes, UNSUPPORTED, 0, 1, 0xFC);
        add_instruction!(opcodes, UNSUPPORTED, 0, 1, 0xFD);
        add_instruction!(opcodes, CP(ArithmeticTarget::D8), 2, 8, 0xFE);
        add_instruction!(opcodes, RST(RstTarget::X38), 1, 16, 0xFF);
        opcodes
    };
    pub static ref PREFIXED_OPCODES: [Option<OpCode>; 256] = {
        use self::Instruction::*;
        let mut opcodes: [Option<OpCode>; 256] = [None; 256];
        add_instruction!(opcodes, RLC(LoadByteTarget::B), 2, 8, 0x00);
        add_instruction!(opcodes, RLC(LoadByteTarget::C), 2, 8, 0x01);
        add_instruction!(opcodes, RLC(LoadByteTarget::D), 2, 8, 0x02);
        add_instruction!(opcodes, RLC(LoadByteTarget::E), 2, 8, 0x03);
        add_instruction!(opcodes, RLC(LoadByteTarget::H), 2, 8, 0x04);
        add_instruction!(opcodes, RLC(LoadByteTarget::L), 2, 8, 0x05);
        add_instruction!(opcodes, RLC(LoadByteTarget::HLI), 2, 16, 0x06);
        add_instruction!(opcodes, RLC(LoadByteTarget::A), 2, 8, 0x07);
        add_instruction!(opcodes, RRC(LoadByteTarget::B), 2, 8, 0x08);
        add_instruction!(opcodes, RRC(LoadByteTarget::C), 2, 8, 0x09);
        add_instruction!(opcodes, RRC(LoadByteTarget::D), 2, 8, 0x0A);
        add_instruction!(opcodes, RRC(LoadByteTarget::E), 2, 8, 0x0B);
        add_instruction!(opcodes, RRC(LoadByteTarget::H), 2, 8, 0x0C);
        add_instruction!(opcodes, RRC(LoadByteTarget::L), 2, 8, 0x0D);
        add_instruction!(opcodes, RRC(LoadByteTarget::HLI), 2, 16, 0x0E);
        add_instruction!(opcodes, RRC(LoadByteTarget::A), 2, 8, 0x0F);
        add_instruction!(opcodes, RL(LoadByteTarget::B), 2, 8, 0x10);
        add_instruction!(opcodes, RL(LoadByteTarget::C), 2, 8, 0x11);
        add_instruction!(opcodes, RL(LoadByteTarget::D), 2, 8, 0x12);
        add_instruction!(opcodes, RL(LoadByteTarget::E), 2, 8, 0x13);
        add_instruction!(opcodes, RL(LoadByteTarget::H), 2, 8, 0x14);
        add_instruction!(opcodes, RL(LoadByteTarget::L), 2, 8, 0x15);
        add_instruction!(opcodes, RL(LoadByteTarget::HLI), 2, 16, 0x16);
        add_instruction!(opcodes, RL(LoadByteTarget::A), 2, 8, 0x17);
        add_instruction!(opcodes, RR(LoadByteTarget::B), 2, 8, 0x18);
        add_instruction!(opcodes, RR(LoadByteTarget::C), 2, 8, 0x19);
        add_instruction!(opcodes, RR(LoadByteTarget::D), 2, 8, 0x1A);
        add_instruction!(opcodes, RR(LoadByteTarget::E), 2, 8, 0x1B);
        add_instruction!(opcodes, RR(LoadByteTarget::H), 2, 8, 0x1C);
        add_instruction!(opcodes, RR(LoadByteTarget::L), 2, 8, 0x1D);
        add_instruction!(opcodes, RR(LoadByteTarget::HLI), 2, 16, 0x1E);
        add_instruction!(opcodes, RR(LoadByteTarget::A), 2, 8, 0x1F);
        add_instruction!(opcodes, SLA(LoadByteTarget::B), 2, 8, 0x20);
        add_instruction!(opcodes, SLA(LoadByteTarget::C), 2, 8, 0x21);
        add_instruction!(opcodes, SLA(LoadByteTarget::D), 2, 8, 0x22);
        add_instruction!(opcodes, SLA(LoadByteTarget::E), 2, 8, 0x23);
        add_instruction!(opcodes, SLA(LoadByteTarget::H), 2, 8, 0x24);
        add_instruction!(opcodes, SLA(LoadByteTarget::L), 2, 8, 0x25);
        add_instruction!(opcodes, SLA(LoadByteTarget::HLI), 2, 16, 0x26);
        add_instruction!(opcodes, SLA(LoadByteTarget::A), 2, 8, 0x27);
        add_instruction!(opcodes, SRA(LoadByteTarget::B), 2, 8, 0x28);
        add_instruction!(opcodes, SRA(LoadByteTarget::C), 2, 8, 0x29);
        add_instruction!(opcodes, SRA(LoadByteTarget::D), 2, 8, 0x2A);
        add_instruction!(opcodes, SRA(LoadByteTarget::E), 2, 8, 0x2B);
        add_instruction!(opcodes, SRA(LoadByteTarget::H), 2, 8, 0x2C);
        add_instruction!(opcodes, SRA(LoadByteTarget::L), 2, 8, 0x2D);
        add_instruction!(opcodes, SRA(LoadByteTarget::HLI), 2, 16, 0x2E);
        add_instruction!(opcodes, SRA(LoadByteTarget::A), 2, 8, 0x2F);
        add_instruction!(opcodes, SWAP(LoadByteTarget::B), 2, 8, 0x30);
        add_instruction!(opcodes, SWAP(LoadByteTarget::C), 2, 8, 0x31);
        add_instruction!(opcodes, SWAP(LoadByteTarget::D), 2, 8, 0x32);
        add_instruction!(opcodes, SWAP(LoadByteTarget::E), 2, 8, 0x33);
        add_instruction!(opcodes, SWAP(LoadByteTarget::H), 2, 8, 0x34);
        add_instruction!(opcodes, SWAP(LoadByteTarget::L), 2, 8, 0x35);
        add_instruction!(opcodes, SWAP(LoadByteTarget::HLI), 2, 16, 0x36);
        add_instruction!(opcodes, SWAP(LoadByteTarget::A), 2, 8, 0x37);
        add_instruction!(opcodes, SRL(LoadByteTarget::B), 2, 8, 0x38);
        add_instruction!(opcodes, SRL(LoadByteTarget::C), 2, 8, 0x39);
        add_instruction!(opcodes, SRL(LoadByteTarget::D), 2, 8, 0x3A);
        add_instruction!(opcodes, SRL(LoadByteTarget::E), 2, 8, 0x3B);
        add_instruction!(opcodes, SRL(LoadByteTarget::H), 2, 8, 0x3C);
        add_instruction!(opcodes, SRL(LoadByteTarget::L), 2, 8, 0x3D);
        add_instruction!(opcodes, SRL(LoadByteTarget::HLI), 2, 16, 0x3E);
        add_instruction!(opcodes, SRL(LoadByteTarget::A), 2, 8, 0x3F);
        add_instruction!(
            opcodes,
            BIT(BitPosition::ZERO, LoadByteTarget::B),
            2,
            8,
            0x40
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::ZERO, LoadByteTarget::C),
            2,
            8,
            0x41
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::ZERO, LoadByteTarget::D),
            2,
            8,
            0x42
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::ZERO, LoadByteTarget::E),
            2,
            8,
            0x43
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::ZERO, LoadByteTarget::H),
            2,
            8,
            0x44
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::ZERO, LoadByteTarget::L),
            2,
            8,
            0x45
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::ZERO, LoadByteTarget::HLI),
            2,
            16,
            0x46
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::ZERO, LoadByteTarget::A),
            2,
            8,
            0x47
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::ONE, LoadByteTarget::B),
            2,
            8,
            0x48
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::ONE, LoadByteTarget::C),
            2,
            8,
            0x49
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::ONE, LoadByteTarget::D),
            2,
            8,
            0x4A
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::ONE, LoadByteTarget::E),
            2,
            8,
            0x4B
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::ONE, LoadByteTarget::H),
            2,
            8,
            0x4C
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::ONE, LoadByteTarget::L),
            2,
            8,
            0x4D
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::ONE, LoadByteTarget::HLI),
            2,
            16,
            0x4E
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::ONE, LoadByteTarget::A),
            2,
            8,
            0x4F
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::TWO, LoadByteTarget::B),
            2,
            8,
            0x50
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::TWO, LoadByteTarget::C),
            2,
            8,
            0x51
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::TWO, LoadByteTarget::D),
            2,
            8,
            0x52
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::TWO, LoadByteTarget::E),
            2,
            8,
            0x53
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::TWO, LoadByteTarget::H),
            2,
            8,
            0x54
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::TWO, LoadByteTarget::L),
            2,
            8,
            0x55
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::TWO, LoadByteTarget::HLI),
            2,
            16,
            0x56
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::TWO, LoadByteTarget::A),
            2,
            8,
            0x57
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::THREE, LoadByteTarget::B),
            2,
            8,
            0x58
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::THREE, LoadByteTarget::C),
            2,
            8,
            0x59
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::THREE, LoadByteTarget::D),
            2,
            8,
            0x5A
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::THREE, LoadByteTarget::E),
            2,
            8,
            0x5B
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::THREE, LoadByteTarget::H),
            2,
            8,
            0x5C
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::THREE, LoadByteTarget::L),
            2,
            8,
            0x5D
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::THREE, LoadByteTarget::HLI),
            2,
            16,
            0x5E
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::THREE, LoadByteTarget::A),
            2,
            8,
            0x5F
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::FOUR, LoadByteTarget::B),
            2,
            8,
            0x60
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::FOUR, LoadByteTarget::C),
            2,
            8,
            0x61
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::FOUR, LoadByteTarget::D),
            2,
            8,
            0x62
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::FOUR, LoadByteTarget::E),
            2,
            8,
            0x63
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::FOUR, LoadByteTarget::H),
            2,
            8,
            0x64
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::FOUR, LoadByteTarget::L),
            2,
            8,
            0x65
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::FOUR, LoadByteTarget::HLI),
            2,
            16,
            0x66
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::FOUR, LoadByteTarget::A),
            2,
            8,
            0x67
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::FIVE, LoadByteTarget::B),
            2,
            8,
            0x68
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::FIVE, LoadByteTarget::C),
            2,
            8,
            0x69
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::FIVE, LoadByteTarget::D),
            2,
            8,
            0x6A
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::FIVE, LoadByteTarget::E),
            2,
            8,
            0x6B
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::FIVE, LoadByteTarget::H),
            2,
            8,
            0x6C
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::FIVE, LoadByteTarget::L),
            2,
            8,
            0x6D
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::FIVE, LoadByteTarget::HLI),
            2,
            16,
            0x6E
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::FIVE, LoadByteTarget::A),
            2,
            8,
            0x6F
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::SIX, LoadByteTarget::B),
            2,
            8,
            0x70
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::SIX, LoadByteTarget::C),
            2,
            8,
            0x71
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::SIX, LoadByteTarget::D),
            2,
            8,
            0x72
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::SIX, LoadByteTarget::E),
            2,
            8,
            0x73
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::SIX, LoadByteTarget::H),
            2,
            8,
            0x74
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::SIX, LoadByteTarget::L),
            2,
            8,
            0x75
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::SIX, LoadByteTarget::HLI),
            2,
            16,
            0x76
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::SIX, LoadByteTarget::A),
            2,
            8,
            0x77
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::SEVEN, LoadByteTarget::B),
            2,
            8,
            0x78
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::SEVEN, LoadByteTarget::C),
            2,
            8,
            0x79
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::SEVEN, LoadByteTarget::D),
            2,
            8,
            0x7A
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::SEVEN, LoadByteTarget::E),
            2,
            8,
            0x7B
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::SEVEN, LoadByteTarget::H),
            2,
            8,
            0x7C
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::SEVEN, LoadByteTarget::L),
            2,
            8,
            0x7D
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::SEVEN, LoadByteTarget::HLI),
            2,
            16,
            0x7E
        );
        add_instruction!(
            opcodes,
            BIT(BitPosition::SEVEN, LoadByteTarget::A),
            2,
            8,
            0x7F
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::ZERO, LoadByteTarget::B),
            2,
            8,
            0x80
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::ZERO, LoadByteTarget::C),
            2,
            8,
            0x81
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::ZERO, LoadByteTarget::D),
            2,
            8,
            0x82
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::ZERO, LoadByteTarget::E),
            2,
            8,
            0x83
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::ZERO, LoadByteTarget::H),
            2,
            8,
            0x84
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::ZERO, LoadByteTarget::L),
            2,
            8,
            0x85
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::ZERO, LoadByteTarget::HLI),
            2,
            16,
            0x86
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::ZERO, LoadByteTarget::A),
            2,
            8,
            0x87
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::ONE, LoadByteTarget::B),
            2,
            8,
            0x88
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::ONE, LoadByteTarget::C),
            2,
            8,
            0x89
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::ONE, LoadByteTarget::D),
            2,
            8,
            0x8A
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::ONE, LoadByteTarget::E),
            2,
            8,
            0x8B
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::ONE, LoadByteTarget::H),
            2,
            8,
            0x8C
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::ONE, LoadByteTarget::L),
            2,
            8,
            0x8D
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::ONE, LoadByteTarget::HLI),
            2,
            16,
            0x8E
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::ONE, LoadByteTarget::A),
            2,
            8,
            0x8F
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::TWO, LoadByteTarget::B),
            2,
            8,
            0x90
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::TWO, LoadByteTarget::C),
            2,
            8,
            0x91
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::TWO, LoadByteTarget::D),
            2,
            8,
            0x92
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::TWO, LoadByteTarget::E),
            2,
            8,
            0x93
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::TWO, LoadByteTarget::H),
            2,
            8,
            0x94
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::TWO, LoadByteTarget::L),
            2,
            8,
            0x95
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::TWO, LoadByteTarget::HLI),
            2,
            16,
            0x96
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::TWO, LoadByteTarget::A),
            2,
            8,
            0x97
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::THREE, LoadByteTarget::B),
            2,
            8,
            0x98
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::THREE, LoadByteTarget::C),
            2,
            8,
            0x99
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::THREE, LoadByteTarget::D),
            2,
            8,
            0x9A
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::THREE, LoadByteTarget::E),
            2,
            8,
            0x9B
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::THREE, LoadByteTarget::H),
            2,
            8,
            0x9C
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::THREE, LoadByteTarget::L),
            2,
            8,
            0x9D
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::THREE, LoadByteTarget::HLI),
            2,
            16,
            0x9E
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::THREE, LoadByteTarget::A),
            2,
            8,
            0x9F
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::FOUR, LoadByteTarget::B),
            2,
            8,
            0xA0
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::FOUR, LoadByteTarget::C),
            2,
            8,
            0xA1
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::FOUR, LoadByteTarget::D),
            2,
            8,
            0xA2
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::FOUR, LoadByteTarget::E),
            2,
            8,
            0xA3
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::FOUR, LoadByteTarget::H),
            2,
            8,
            0xA4
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::FOUR, LoadByteTarget::L),
            2,
            8,
            0xA5
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::FOUR, LoadByteTarget::HLI),
            2,
            16,
            0xA6
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::FOUR, LoadByteTarget::A),
            2,
            8,
            0xA7
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::FIVE, LoadByteTarget::B),
            2,
            8,
            0xA8
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::FIVE, LoadByteTarget::C),
            2,
            8,
            0xA9
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::FIVE, LoadByteTarget::D),
            2,
            8,
            0xAA
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::FIVE, LoadByteTarget::E),
            2,
            8,
            0xAB
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::FIVE, LoadByteTarget::H),
            2,
            8,
            0xAC
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::FIVE, LoadByteTarget::L),
            2,
            8,
            0xAD
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::FIVE, LoadByteTarget::HLI),
            2,
            16,
            0xAE
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::FIVE, LoadByteTarget::A),
            2,
            8,
            0xAF
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::SIX, LoadByteTarget::B),
            2,
            8,
            0xB0
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::SIX, LoadByteTarget::C),
            2,
            8,
            0xB1
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::SIX, LoadByteTarget::D),
            2,
            8,
            0xB2
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::SIX, LoadByteTarget::E),
            2,
            8,
            0xB3
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::SIX, LoadByteTarget::H),
            2,
            8,
            0xB4
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::SIX, LoadByteTarget::L),
            2,
            8,
            0xB5
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::SIX, LoadByteTarget::HLI),
            2,
            16,
            0xB6
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::SIX, LoadByteTarget::A),
            2,
            8,
            0xB7
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::SEVEN, LoadByteTarget::B),
            2,
            8,
            0xB8
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::SEVEN, LoadByteTarget::C),
            2,
            8,
            0xB9
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::SEVEN, LoadByteTarget::D),
            2,
            8,
            0xBA
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::SEVEN, LoadByteTarget::E),
            2,
            8,
            0xBB
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::SEVEN, LoadByteTarget::H),
            2,
            8,
            0xBC
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::SEVEN, LoadByteTarget::L),
            2,
            8,
            0xBD
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::SEVEN, LoadByteTarget::HLI),
            2,
            16,
            0xBE
        );
        add_instruction!(
            opcodes,
            RESET(BitPosition::SEVEN, LoadByteTarget::A),
            2,
            8,
            0xBF
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::ZERO, LoadByteTarget::B),
            2,
            8,
            0xC0
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::ZERO, LoadByteTarget::C),
            2,
            8,
            0xC1
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::ZERO, LoadByteTarget::D),
            2,
            8,
            0xC2
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::ZERO, LoadByteTarget::E),
            2,
            8,
            0xC3
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::ZERO, LoadByteTarget::H),
            2,
            8,
            0xC4
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::ZERO, LoadByteTarget::L),
            2,
            8,
            0xC5
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::ZERO, LoadByteTarget::HLI),
            2,
            16,
            0xC6
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::ZERO, LoadByteTarget::A),
            2,
            8,
            0xC7
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::ONE, LoadByteTarget::B),
            2,
            8,
            0xC8
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::ONE, LoadByteTarget::C),
            2,
            8,
            0xC9
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::ONE, LoadByteTarget::D),
            2,
            8,
            0xCA
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::ONE, LoadByteTarget::E),
            2,
            8,
            0xCB
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::ONE, LoadByteTarget::H),
            2,
            8,
            0xCC
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::ONE, LoadByteTarget::L),
            2,
            8,
            0xCD
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::ONE, LoadByteTarget::HLI),
            2,
            16,
            0xCE
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::ONE, LoadByteTarget::A),
            2,
            8,
            0xCF
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::TWO, LoadByteTarget::B),
            2,
            8,
            0xD0
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::TWO, LoadByteTarget::C),
            2,
            8,
            0xD1
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::TWO, LoadByteTarget::D),
            2,
            8,
            0xD2
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::TWO, LoadByteTarget::E),
            2,
            8,
            0xD3
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::TWO, LoadByteTarget::H),
            2,
            8,
            0xD4
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::TWO, LoadByteTarget::L),
            2,
            8,
            0xD5
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::TWO, LoadByteTarget::HLI),
            2,
            16,
            0xD6
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::TWO, LoadByteTarget::A),
            2,
            8,
            0xD7
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::THREE, LoadByteTarget::B),
            2,
            8,
            0xD8
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::THREE, LoadByteTarget::C),
            2,
            8,
            0xD9
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::THREE, LoadByteTarget::D),
            2,
            8,
            0xDA
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::THREE, LoadByteTarget::E),
            2,
            8,
            0xDB
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::THREE, LoadByteTarget::H),
            2,
            8,
            0xDC
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::THREE, LoadByteTarget::L),
            2,
            8,
            0xDD
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::THREE, LoadByteTarget::HLI),
            2,
            16,
            0xDE
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::THREE, LoadByteTarget::A),
            2,
            8,
            0xDF
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::FOUR, LoadByteTarget::B),
            2,
            8,
            0xE0
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::FOUR, LoadByteTarget::C),
            2,
            8,
            0xE1
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::FOUR, LoadByteTarget::D),
            2,
            8,
            0xE2
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::FOUR, LoadByteTarget::E),
            2,
            8,
            0xE3
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::FOUR, LoadByteTarget::H),
            2,
            8,
            0xE4
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::FOUR, LoadByteTarget::L),
            2,
            8,
            0xE5
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::FOUR, LoadByteTarget::HLI),
            2,
            16,
            0xE6
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::FOUR, LoadByteTarget::A),
            2,
            8,
            0xE7
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::FIVE, LoadByteTarget::B),
            2,
            8,
            0xE8
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::FIVE, LoadByteTarget::C),
            2,
            8,
            0xE9
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::FIVE, LoadByteTarget::D),
            2,
            8,
            0xEA
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::FIVE, LoadByteTarget::E),
            2,
            8,
            0xEB
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::FIVE, LoadByteTarget::H),
            2,
            8,
            0xEC
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::FIVE, LoadByteTarget::L),
            2,
            8,
            0xED
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::FIVE, LoadByteTarget::HLI),
            2,
            16,
            0xEE
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::FIVE, LoadByteTarget::A),
            2,
            8,
            0xEF
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::SIX, LoadByteTarget::B),
            2,
            8,
            0xF0
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::SIX, LoadByteTarget::C),
            2,
            8,
            0xF1
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::SIX, LoadByteTarget::D),
            2,
            8,
            0xF2
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::SIX, LoadByteTarget::E),
            2,
            8,
            0xF3
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::SIX, LoadByteTarget::H),
            2,
            8,
            0xF4
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::SIX, LoadByteTarget::L),
            2,
            8,
            0xF5
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::SIX, LoadByteTarget::HLI),
            2,
            16,
            0xF6
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::SIX, LoadByteTarget::A),
            2,
            8,
            0xF7
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::SEVEN, LoadByteTarget::B),
            2,
            8,
            0xF8
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::SEVEN, LoadByteTarget::C),
            2,
            8,
            0xF9
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::SEVEN, LoadByteTarget::D),
            2,
            8,
            0xFA
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::SEVEN, LoadByteTarget::E),
            2,
            8,
            0xFB
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::SEVEN, LoadByteTarget::H),
            2,
            8,
            0xFC
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::SEVEN, LoadByteTarget::L),
            2,
            8,
            0xFD
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::SEVEN, LoadByteTarget::HLI),
            2,
            16,
            0xFE
        );
        add_instruction!(
            opcodes,
            SET(BitPosition::SEVEN, LoadByteTarget::A),
            2,
            8,
            0xFF
        );
        opcodes
    };
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum ArithmeticTarget {
    A,
    B,
    C,
    D,
    E,
    H,
    L,
    D8,
    HLI,
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Arithmetic16Target {
    BC,
    DE,
    HL,
    SP,
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum BitPosition {
    ZERO = 0,
    ONE = 1,
    TWO = 2,
    THREE = 3,
    FOUR = 4,
    FIVE = 5,
    SIX = 6,
    SEVEN = 7,
}

#[derive(Copy, Clone, Debug, PartialEq)]
/// All the valid targets for incrementing/decrementing.
/// They're slightly different from ArithmeticTarget in that
/// incrementing/decrementing immediate values doesn't make any sense.
pub enum IncDecTarget {
    A,
    B,
    C,
    D,
    E,
    H,
    L,
    HLI,
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum IncDec16Target {
    BC,
    DE,
    HL,
    SP,
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum JumpTest {
    NotZero,
    Zero,
    NotCarry,
    Carry,
    Always,
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum LoadType {
    Byte(LoadByteTarget, LoadByteSource),
    HighFromA(LoadHighByteTarget),
    HighToA(LoadHighByteTarget),
    IndirectByteFromA(LoadIndirectByteTarget),
    IndirectByteToA(LoadIndirectByteTarget),
    HlSpE,
    SpHl,
    Word(LoadWordTarget, LoadWordSource),
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum LoadByteTarget {
    A,
    B,
    C,
    D,
    E,
    H,
    L,
    HLI,
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum LoadByteSource {
    A,
    B,
    C,
    D,
    E,
    H,
    L,
    D8,
    HLI,
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum LoadHighByteTarget {
    C,
    D8,
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum LoadIndirectByteTarget {
    A16,
    BC,
    DE,
    HlInc,
    HlDec,
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum LoadWordTarget {
    BC,
    DE,
    HL,
    SP,
    I16,
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum LoadWordSource {
    D16,
    SP,
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum RstTarget {
    X00,
    X10,
    X20,
    X30,
    X08,
    X18,
    X28,
    X38,
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum StackTarget {
    BC,
    DE,
    HL,
    AF,
}

impl OpCode {
    pub fn from_byte(byte: u8, is_prefxed: bool) -> Option<OpCode> {
        if is_prefxed {
            PREFIXED_OPCODES[byte as usize]
        } else {
            NON_PREFIXED_OPCODES[byte as usize]
        }
    }

    pub fn from_instruction(instruction: Instruction) -> OpCode {
        if let Some(opcode) = NON_PREFIXED_OPCODES
            .iter()
            .filter_map(|&opcode| opcode)
            .find(|&opcode| opcode.instruction == instruction)
        {
            opcode
        } else if let Some(opcode) = PREFIXED_OPCODES
            .iter()
            .filter_map(|&opcode| opcode)
            .find(|&opcode| opcode.instruction == instruction)
        {
            opcode
        } else {
            panic!("{:?} is not yet in the table", instruction);
        }
    }
}

impl Instruction {
    pub fn from_byte(byte: u8, is_prefixed: bool) -> Option<Instruction> {
        if is_prefixed {
            Instruction::from_byte_prefixed(byte)
        } else {
            Instruction::from_byte_non_prefixed(byte)
        }
    }

    fn from_byte_non_prefixed(byte: u8) -> Option<Instruction> {
        match NON_PREFIXED_OPCODES[byte as usize] {
            Some(opcode) => Some(opcode.instruction),
            None => None,
        }
    }

    fn from_byte_prefixed(_byte: u8) -> Option<Instruction> {
        None
    }
}
